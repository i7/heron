mkdir bundle || { echo "Aborting..." ; exit 1; }

# documentation
cp LICENCE bundle/
cp README.md bundle/

# code
cp *.py bundle/
cp -r templates bundle/

# dependency
cp processnet.zip bundle/

# examples
cp -r examples bundle/

# transparency of data generation
cp result.csv bundle/
cp -r .logs bundle/
cp run-examples.sh bundle/
cp build-artifact.sh bundle/

zip -r heron.zip bundle

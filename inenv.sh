#!/bin/env bash
. /opt/anaconda/bin/activate heron
PYTHONPATH="$PYTHONPATH:../:" PYTHONASYNCIODEBUG=1 eval "$@"
conda deactivate

import system as sys
import dataclasses
import typing
import itertools
import types

from processnet import net as pn  # type: ignore
from processnet import algorithm as pn_alg  # type: ignore


class IterationPlace(typing.NamedTuple):
    transition: sys.IteratingTransition
    agent: int
    value: int

    @property
    def pretty(self) -> str:
        return f"iteration {self.agent}[{self.value}]"


class LocalVariablePlace(typing.NamedTuple):
    variable: sys.LocalVariable
    value: sys.Value
    agent: int

    @property
    def pretty(self) -> str:
        return f"{self.variable.pretty}[{self.agent}] = {self.value.name}"


class PointerPlace(typing.NamedTuple):
    pointer: sys.Pointer
    value: int

    @property
    def pretty(self) -> str:
        return f"{self.pointer.pretty} = {self.value}"


@dataclasses.dataclass(frozen=True)
class Instance:
    system: sys.System
    size: int

    bot: int = dataclasses.field(init=False, default=-1)

    def __post_init__(self):
        object.__setattr__(self, "_top", self.size)
        object.__setattr__(self,
                           "_iterating_transitions",
                           tuple(t for t in self.system.transitions
                                 if isinstance(t, sys.IteratingTransition)))
        object.__setattr__(self,
                           "_local_transitions",
                           tuple(t for t in self.system.transitions
                                 if isinstance(t, sys.LocalTransition)))
        flows: typing.Set[pn.PlaceSet] = set()
        for agent in range(self.size):
            for var in self.system.store.local_vars:
                if var == self.system.store.state_var:
                    continue
                flows.add(
                        frozenset({
                            LocalVariablePlace(var, val, agent)
                            for val in var.values}))
            flows.add(
                    frozenset({
                        IterationPlace(t, agent, value)
                        for t in self.iterating_transitions
                        for value in (set(range(self.size))
                                      | {self.bot, self.top})}
                        | {
                        LocalVariablePlace(self.system.store.state_var,
                                           val,
                                           agent)
                        for val in self.system.store.state_var.values}))
        for ptr in self.system.store.pointers:
            flows.add(
                    frozenset({
                        PointerPlace(ptr, agent)
                        for agent in range(self.size)}))
        object.__setattr__(self,
                           "_instance_flows",
                           frozenset(flows))

        places = {
                LocalVariablePlace(var, val, agent)
                for var in self.system.store.local_vars
                for val in var.values
                for agent in range(self.size)
            } | {
                PointerPlace(ptr, agent)
                for ptr in self.system.store.pointers
                for agent in range(self.size)
            } | {
                IterationPlace(t, agent, value)
                for t in self.iterating_transitions
                for agent in range(self.size)
                for value in set(range(self.size)) | {self.bot, self.top}
            }


        def _state_place(agent: int, state: str) -> LocalVariablePlace:
            vals = [v
                    for v in self.system.store.state_var.values
                    if v.name == state]
            assert len(vals) == 1
            val, = vals
            return LocalVariablePlace(self.system.store.state_var, val, agent)

        def _fill_inter(
                inter: sys.Interpretation,
                ptrs: typing.Iterable[sys.Pointer],
                lterms: typing.Iterable[sys.LocalTerm]
                ) -> typing.FrozenSet[sys.Interpretation]:
            all_ptrs = tuple(
                    (set(ptrs) | {lterm.index for lterm in lterms})
                    - set(inter.map_ptr.keys())
                    )
            all_lterms = tuple(set(lterms) - set(inter.map_local.keys()))
            ptrs_assignments = {
                    inter.pointer_assignments + tuple(zip(all_ptrs, values))
                    for values in itertools.product(range(self.size),
                                                    repeat=len(all_ptrs))}
            lterms_assignments = {
                    inter.local_assignments + tuple(zip(all_lterms, values))
                    for values in itertools.product(*(l.base.values
                                                      for l in all_lterms))}
            return frozenset(
                    (sys.Interpretation(l, p)
                     for p, l in itertools.product(ptrs_assignments,
                                                   lterms_assignments)))

        def _fill_all_inter(
                base: typing.FrozenSet[sys.Interpretation],
                ptrs: typing.Iterable[sys.Pointer],
                lterms: typing.Iterable[sys.LocalTerm]
                ) -> typing.FrozenSet[sys.Interpretation]:
            result: typing.Set[sys.Interpretation] = set()
            for b in base:
                result |= _fill_inter(b, ptrs, lterms)
            return frozenset(result)

        def _sep_inter(
                interpretations: typing.FrozenSet[sys.Interpretation],
                guard: typing.Tuple[sys.Comparison, ...]
                ) -> typing.Tuple[typing.FrozenSet[sys.Interpretation],
                                  typing.FrozenSet[sys.Interpretation]]:
            sat: typing.Set[sys.Interpretation] = set()
            unsat: typing.Set[sys.Interpretation] = set()
            for assg in interpretations:
                if all(comp.evaluate(assg) for comp in guard):
                    sat.add(assg)
                else:
                    unsat.add(assg)
            return frozenset(sat), frozenset(unsat)

        def _instantiate_guarded_transition(
                ctx: sys.Interpretation,
                local_ptr: sys.Pointer,
                src: typing.Union[LocalVariablePlace, IterationPlace],
                target: typing.Union[LocalVariablePlace, IterationPlace],
                variable_assignments: typing.Tuple[
                    typing.Tuple[sys.LocalVariable, sys.Value], ...],
                pointer_assignments: typing.Tuple[
                    typing.Tuple[sys.Pointer, sys.Pointing], ...],
                guard: typing.Tuple[sys.Comparison, ...]
                ) -> typing.FrozenSet[pn.Transition]:
            var_map = {var: val for var, val in variable_assignments}
            ptr_map = {lptr: rptr for lptr, rptr in pointer_assignments}
            ptrs: typing.Set[sys.Pointer] = set()
            derived_dec_ptrs: typing.Set[sys.Moddec] = set()
            derived_inc_ptrs: typing.Set[sys.Modinc] = set()
            lterms: typing.Set[sys.LocalTerm] = set()
            for ptr_l, ptr_r in pointer_assignments:
                ptrs.add(ptr_l)
                if isinstance(ptr_r, sys.Moddec):
                    derived_dec_ptrs.add(ptr_r)
                elif isinstance(ptr_r, sys.Modinc):
                    derived_inc_ptrs.add(ptr_r)
                else:
                    ptrs.add(ptr_r)
            for var, _ in variable_assignments:
                lterms.add(sys.LocalTerm(var, local_ptr))

            assignments = _fill_inter(ctx, ptrs, lterms)
            assignments = frozenset(
                    (sys.Interpretation(
                        inter.local_assignments,
                        (inter.pointer_assignments
                         + tuple((dec_ptr,
                                  self.size-1
                                  if inter.map_ptr[dec_ptr.ptr] == 0
                                  else inter.map_ptr[dec_ptr.ptr]-1)
                                 for dec_ptr in derived_dec_ptrs)
                         + tuple((inc_ptr,
                                  0
                                  if inter.map_ptr[inc_ptr.ptr] == self.size-1
                                  else inter.map_ptr[inc_ptr.ptr]-1)
                                 for inc_ptr in derived_inc_ptrs)))
                     for inter in assignments))
            def _add(what: typing.Union[sys.Pointer,
                                        sys.LocalTerm,
                                        sys.Value]) -> None:
                if isinstance(what, sys.Pointer):
                    ptrs.add(what)
                if isinstance(what, sys.LocalTerm):
                    lterms.add(what)
                    ptrs.add(what.index)
            for comp in guard:
                _add(comp.left)
                _add(comp.right)
            sat, _ = _sep_inter(_fill_all_inter(assignments, ptrs, lterms),
                                guard)
            result: typing.Set[pn.Transition] = set()
            for assignment in sat:
                preset = frozenset({src}
                         | {
                             LocalVariablePlace(
                                 lterm.base,
                                 assignment.map_local[lterm],
                                 assignment.map_ptr[lterm.index])
                             for lterm in assignment.map_local}
                         | {
                             PointerPlace(
                                 ptr,
                                 assignment.map_ptr[ptr])
                             for ptr in assignment.map_ptr
                             if (ptr not in sys.ReservedPointer
                                 and not isinstance(ptr, sys.Moddec)
                                 and not isinstance(ptr, sys.Modinc))})
                postset = frozenset(
                        {target}
                        | {
                            LocalVariablePlace(
                                lterm.base,
                                assignment.map_local[lterm],
                                assignment.map_ptr[lterm.index])
                            for lterm in assignment.map_local
                            if lterm.base not in var_map}
                        | {
                            LocalVariablePlace(
                                lterm.base,
                                var_map[lterm.base],
                                assignment.map_ptr[lterm.index])
                            for lterm in assignment.map_local
                            if lterm.base in var_map}
                        | {
                            PointerPlace(
                                ptr,
                                assignment.map_ptr[ptr])
                            for ptr in assignment.map_ptr
                            if (ptr not in ptr_map
                                and ptr not in sys.ReservedPointer
                                and not isinstance(ptr, sys.Moddec)
                                and not isinstance(ptr, sys.Modinc))}
                        | {
                            PointerPlace(
                                ptr,
                                assignment.map_ptr[ptr_map[ptr]])
                            for ptr in assignment.map_ptr
                            if (ptr in ptr_map
                                and ptr not in sys.ReservedPointer
                                and not isinstance(ptr, sys.Moddec)
                                and not isinstance(ptr, sys.Modinc))})
                result.add(
                        pn.Transition(
                            preset,
                            postset))
            return frozenset(result)

        def _instantiate_iteration_transition_exit(
                agent: int,
                other: int,
                t: sys.IteratingTransition,
                exit: sys.IterationExit) -> typing.FrozenSet[pn.Transition]:
            src = IterationPlace(t, agent, other)
            target = _state_place(agent, exit.target)
            ctx = sys.Interpretation(tuple(),
                                     ((sys.self_ptr, agent),
                                      (sys.other_ptr, other)))
            return _instantiate_guarded_transition(
                    ctx,
                    sys.other_ptr,
                    src,
                    target,
                    exit.variable_assignments,
                    exit.pointer_assignments,
                    exit.guard)

        def _instantiate_iteration_first_step(
                t: sys.IteratingTransition) -> typing.FrozenSet[pn.Transition]:
            return frozenset(
                    pn.Transition(
                        frozenset({
                            IterationPlace(
                                t,
                                agent,
                                self.bot
                                if t.direction == sys.IterationDirection.UP
                                else self.top)}),
                        frozenset({
                            IterationPlace(
                                t,
                                agent,
                                0
                                if t.direction == sys.IterationDirection.UP
                                else self.top-1)}))
                    for agent in range(self.size))

        def _instantiate_iteration_begin(
                t: sys.IteratingTransition) -> typing.FrozenSet[pn.Transition]:
            result: typing.Set[pn.Transition] = set()
            for agent in range(self.size):
                initial_src = _state_place(agent, t.src)
                # we add this regardless of a defined start
                if t.start is None:
                    # undefined start implicitly gives top or bot
                    result.add(
                            pn.Transition(
                                frozenset({initial_src}),
                                frozenset({
                                    IterationPlace(
                                        t,
                                        agent,
                                        self.bot
                                        if t.direction == sys.IterationDirection.UP
                                        else self.top)})))
                elif t.start is sys.self_ptr:
                    # self start is specific
                    result.add(
                            pn.Transition(
                                frozenset({initial_src}),
                                frozenset({
                                    IterationPlace(
                                        t,
                                        agent,
                                        agent)})))
                else:
                    # explicit start needs observation
                    for start_val in range(self.size):
                        initial_target = IterationPlace(
                                t,
                                agent,
                                start_val)
                        obs = PointerPlace(
                                t.start,
                                start_val)
                        result.add(
                                pn.Transition(
                                    frozenset({initial_src, obs}),
                                    frozenset({initial_target, obs})))
            return frozenset(result)

        def _instantiate_iteration_exits(
                t: sys.IteratingTransition
                ) -> typing.Tuple[typing.FrozenSet[pn.Transition], ...]:
            # exits
            results: typing.List[typing.FrozenSet[pn.Transition]] = []
            for exit in t.exits:
                result: typing.Set[pn.Transition] = set()
                for agent in range(self.size):
                    if not exit.guard:
                        # exceeded is special
                        src = IterationPlace(
                                t,
                                agent,
                                self.bot
                                if t.direction == sys.IterationDirection.DOWN
                                else self.top)
                        target = _state_place(agent, exit.target)
                        result.add(
                                pn.Transition(
                                    frozenset({src}),
                                    frozenset({target})))
                    else:
                        for other in range(self.size):
                            result |= _instantiate_iteration_transition_exit(
                                    agent,
                                    other,
                                    t,
                                    exit)
                results.append(frozenset(result))
            return tuple(results)

        def _instantiate_iteration_progress(
                t: sys.IteratingTransition
                ) -> typing.Tuple[typing.FrozenSet[pn.Transition], ...]:
            results: typing.List[typing.FrozenSet[pn.Transition]] = []
            for progress_guard in t.progress_guards:
                result: typing.Set[pn.Transition] = set()
                for agent in range(self.size):
                    for other in range(self.size):
                        src = IterationPlace(
                                t,
                                agent,
                                other)
                        progress_target = IterationPlace(
                                t,
                                agent,
                                other+1
                                if t.direction == sys.IterationDirection.UP
                                else other-1)
                        ctx = sys.Interpretation(
                                tuple(),
                                ((sys.self_ptr, agent),
                                 (sys.other_ptr, other)))
                        result |= _instantiate_guarded_transition(
                                ctx,
                                sys.other_ptr,
                                src,
                                progress_target,
                                tuple(),
                                tuple(),
                                progress_guard)
                results.append(frozenset(result))
            return tuple(results)

        def _instantiate_local_transition(
                t: sys.LocalTransition) -> typing.FrozenSet[pn.Transition]:
            result: typing.Set[pn.Transition] = set()
            for agent in range(self.size):
                src = _state_place(agent, t.src)
                target = _state_place(agent, t.target)
                ctx = sys.Interpretation(tuple(),
                                         ((sys.self_ptr, agent),))
                result |= _instantiate_guarded_transition(
                        ctx,
                        sys.self_ptr,
                        src,
                        target,
                        t.variable_assignments,
                        t.pointer_assignments,
                        t.guard)
            return frozenset(result)

        local_transitions: typing.Mapping[sys.LocalTransition,
                                          typing.FrozenSet[pn.Transition]] = {
            transition: _instantiate_local_transition(transition)
            for transition in self.local_transitions}
        iterating_transitions: typing.Mapping[
                sys.IteratingTransition,
                typing.Tuple[
                    typing.FrozenSet[pn.Transition],
                    typing.FrozenSet[pn.Transition],
                    typing.Tuple[typing.FrozenSet[pn.Transition], ...],
                    typing.Tuple[typing.FrozenSet[pn.Transition], ...]]] = {
                            t: (_instantiate_iteration_begin(t),
                                _instantiate_iteration_first_step(t),
                                _instantiate_iteration_exits(t),
                                _instantiate_iteration_progress(t))
                            for t in self.iterating_transitions}
        transitions: typing.Set[pn.Transition] = set()
        for k in local_transitions:
            transitions |= local_transitions[k]
        for k in iterating_transitions:
            begin, first, exits, progress = iterating_transitions[k]
            transitions |= begin
            transitions |= first
            for exit in exits:
                transitions |= exit
            for p in progress:
                transitions |= p

        initially_marked_places = frozenset(
                {
                    LocalVariablePlace(
                        var,
                        var.values[0],
                        i)
                    for var in self.system.store.local_vars
                    for i in range(self.size)}
                | {
                    PointerPlace(
                        ptr,
                        0)
                    for ptr in self.system.store.pointers})

        object.__setattr__(self,
                           "_local_transition_instances",
                           types.MappingProxyType(local_transitions))

        object.__setattr__(self,
                           "_iterating_transition_instances",
                           types.MappingProxyType(iterating_transitions))

        object.__setattr__(self,
                           "_initial_marking",
                           pn.Marking(
                               pn.Processnet(
                                   frozenset(places),
                                   tuple(transitions)),
                               initially_marked_places))

        def _unsafe_marked_places(
                prop: sys.UnsafeProperty
                ) -> typing.Tuple[typing.FrozenSet[LocalVariablePlace], ...]:
            res: typing.List[typing.Set[LocalVariablePlace]] = []
            universals = (prop.inbetweens
                          if prop.inbetweens
                          else [tuple()]*(len(prop.existentials)+1))
            for existential_indices in itertools.combinations(
                    range(self.size),
                    len(prop.existentials)):
                config_at_index: typing.List[
                        typing.Tuple[
                            typing.Tuple[sys.LocalVariable, sys.Value],
                            ...]] = []
                enriched_indices = (-1,) + existential_indices + (self.size,)
                for i in range(len(enriched_indices)-1):
                    lower = enriched_indices[i]
                    upper = enriched_indices[i+1]
                    config_at_index += [universals[i]]*(upper-lower-1)
                    if upper < self.size:
                        config_at_index.append(prop.existentials[i])
                current: typing.Set[LocalVariablePlace] = set()
                for i in range(self.size):
                    current_config = config_at_index
                    current |= {LocalVariablePlace(var, val, i)
                                for var, val in config_at_index[i]}
                res.append(current)
            return tuple(frozenset(v) for v in res)

        object.__setattr__(self,
                           "_property_markings",
                           types.MappingProxyType(
                               {prop.name: tuple(
                                   pn.Marking(self.initial_marking.net, m)
                                   for m in _unsafe_marked_places(prop))
                                for prop in self.system.properties}))

    @property
    def flows(self) -> typing.FrozenSet[pn.PlaceSet]:
        return self._instance_flows  # type: ignore

    @property
    def initial_marking(self) -> pn.Marking:
        return self._initial_marking  # type: ignore

    def bad_markings(self, prop_name: str) -> typing.Tuple[pn.Marking, ...]:
        return self._property_markings[prop_name]  # type: ignore

    def get_local_transition_instances(
            self,
            transition: sys.LocalTransition
            ) -> typing.FrozenSet[pn.Transition]:
        return self._local_transition_instances[transition]  # type: ignore

    def get_iterating_transition_instances(
            self,
            transition: sys.IteratingTransition
            ) -> typing.Tuple[
                    typing.FrozenSet[pn.Transition],                      # instances for the begin of the iteration
                    typing.FrozenSet[pn.Transition],                      # instances for the first step
                    typing.Tuple[typing.FrozenSet[pn.Transition], ...],   # instances for exits
                    typing.Tuple[typing.FrozenSet[pn.Transition], ...]]:  # instances for progress
        return self._iterating_transition_instances[transition]  # type: ignore

    @property
    def iterating_transitions(self) -> typing.Tuple[sys.IteratingTransition,
                                                    ...]:
        return self._iterating_transitions  # type: ignore

    @property
    def local_transitions(self) -> typing.Tuple[sys.LocalTransition, ...]:
        return self._local_transitions  # type: ignore

    @property
    def top(self) -> int:
        return self._top  # type: ignore

import logging
import typing
import json
import system
import lark  # type: ignore


logger = logging.getLogger(__name__)


def parse_local_variable_spec(json_var) -> system.LocalVariable:
    assert json_var["type"] == "local"
    name = json_var["name"]
    if name in (system.ReservedPointer
                | set(system.ReservedVariableNames)):
        raise ValueError(f"Cannot declare a local variable with name {name}")
    values = tuple(system.Value(name, i)
                   for i, name in enumerate(json_var["values"]))
    if not all(isinstance(v.name, str) for v in values):
        raise ValueError(f"Unable to parse values {json_var['values']}")
    return system.LocalVariable(name, values)


def parse_pointer_spec(json_var) -> system.Pointer:
    assert json_var["type"] == "pointer"
    name = json_var["name"]
    if name in (system.ReservedPointer
                | set(system.ReservedVariableNames)):
        raise ValueError(f"Cannot declare a pointer variable with name {name}")
    return system.Pointer(name)


def parse_states_spec(states: typing.Tuple[str, ...]) -> system.LocalVariable:
    return system.LocalVariable("state",
                                tuple(system.Value(s, i)
                                      for i, s in enumerate(states)))


def parse_variable_store(json_store,
                         state_var: system.LocalVariable
                         ) -> system.VariableStore:
    pointers = tuple(parse_pointer_spec(pointer_spec)
                     for pointer_spec in json_store
                     if pointer_spec["type"] == "pointer")
    local_variables = tuple(parse_local_variable_spec(var_spec)
                            for var_spec in json_store
                            if var_spec["type"] == "local") + (state_var,)
    if shared_names := ({t.name for t in pointers + local_variables
                         if any(t.name == o.name
                                for o in pointers + local_variables
                                if t != o)}):
        raise ValueError("Names <{names}> occur multiple times".format(
            names=", ".join(sorted(shared_names))))
    return system.VariableStore(
            pointers,
            local_variables,
            state_var)


formula_grammar = '''
term : CNAME               -> raw_term
     | CNAME "[" CNAME "]" -> indexed

comparison : term "<" term  -> less
           | term "<=" term -> less_equal
           | term ">" term  -> greater
           | term ">=" term -> greater_equal
           | term "==" term -> equal
           | term "!=" term -> unequal

%import common.WS
%ignore WS
%import common.CNAME
'''


comparison_parser = lark.Lark(formula_grammar, start="comparison")


class ComparisonTransformer(lark.Transformer):
    def parse_terms(self, term_left, term_right):
        def _ordered(spec_term, derived_term):
            if spec_term.data == 'indexed':
                base, index = [str(c) for c in spec_term.children]
                base_var = self.store.get_var(base)
                assert isinstance(base_var, system.LocalVariable)
                if index == "self":
                    index_var = system.self_ptr
                elif index == "other":
                    index_var = system.other_ptr
                else:
                    _index_var = self.store.get_var(index)
                    assert isinstance(_index_var, system.Pointer)
                    index_var = _index_var
                left = system.LocalTerm(base_var, index_var)
                assert derived_term.data == "raw_term"
                derived_name, = [str(c) for c in derived_term.children]
                derived_val, = [v for v in left.base.values
                                if v.name == derived_name]
                return left, derived_val
            elif spec_term.data == 'raw_term':
                name, = [str(c) for c in spec_term.children]
                try:
                    var = self.store.get_var(name)
                except KeyError:
                    if name == "self":
                        var = system.self_ptr
                    elif name == "other":
                        var = system.other_ptr
                    else:
                        return None
                if isinstance(var, system.LocalVariable):
                    assert derived_term.data == "raw_term"
                    derived_name, = [str(c) for c in derived_term.children]
                    derived_val, = [v for v in var.values
                                    if v.name == derived_name]
                    l_var = system.LocalTerm(
                            var,
                            self.local_ctx)
                    return l_var, derived_val
                elif isinstance(var, system.Pointer):
                    assert derived_term.data == "raw_term"
                    derived_name, = [str(c) for c in derived_term.children]
                    if derived_name == "self":
                        derived_ptr = system.self_ptr
                    elif derived_name == "other":
                        derived_ptr = system.other_ptr
                    elif derived_name == "moddec":
                        derived_ptr = system.Moddec(var)
                    elif derived_name == "modinc":
                        derived_ptr = system.Modinc(var)
                    else:
                        _d_ptr = self.store.get_var(derived_name)
                        assert isinstance(_d_ptr, system.Pointer)
                        derived_ptr = _d_ptr
                    return var, derived_ptr
                else:
                    return None
        res = _ordered(term_left, term_right)
        if res is None:
            res = _ordered(term_right, term_left)
        if res is None:
            raise ValueError(f"Cannot parse comp. with terms <{term_left}, "
                             f"{term_right}>")
        return res

    @lark.v_args(inline=True)
    def less(self, left_term, right_term):
        l, r = self.parse_terms(left_term, right_term)
        return system.Less(l, r)

    @lark.v_args(inline=True)
    def less_equal(self, left_term, right_term):
        l, r = self.parse_terms(left_term, right_term)
        return system.LessEqual(l, r)

    @lark.v_args(inline=True)
    def greater(self, left_term, right_term):
        l, r = self.parse_terms(left_term, right_term)
        return system.Less(r, l)

    @lark.v_args(inline=True)
    def greater_equal(self, left_term, right_term):
        l, r = self.parse_terms(left_term, right_term)
        return system.LessEqual(r, l)

    @lark.v_args(inline=True)
    def equal(self, left_term, right_term):
        l, r = self.parse_terms(left_term, right_term)
        return system.Equal(l, r)

    @lark.v_args(inline=True)
    def unequal(self, left_term, right_term):
        l, r = self.parse_terms(left_term, right_term)
        return system.Unequal(l, r)


def parse_comparison(comp_interpreter: ComparisonTransformer,
                     expr: str) -> system.Comparison:
    tree = comparison_parser.parse(expr)
    return comp_interpreter.transform(tree)


def parse_assignment(
        store: system.VariableStore,
        assignment_json
        ) -> typing.Tuple[
                typing.Tuple[typing.Tuple[system.LocalVariable, system.Value],
                             ...],
                typing.Tuple[typing.Tuple[system.Pointer, system.Pointing],
                             ...]]:
    local_assignments: typing.List[typing.Tuple[system.LocalVariable,
                                                system.Value]] = []
    pointer_assignments: typing.List[typing.Tuple[system.Pointer,
                                                  system.Pointing]] = []
    for key, val in assignment_json.items():
        var = store.get_var(key)
        if var.var_type == system.VariableType.LOCAL:
            assert isinstance(var, system.LocalVariable)
            if len(values := {v for v in var.values if v.name == val}) == 0:
                raise ValueError(f"No value for {var} has name {val}")
            elif len(values) > 1:
                raise ValueError(f"Too many values for {var} have name {val}")
            value, = values
            local_assignments.append((var, value))
        elif var.var_type == system.VariableType.POINTER:
            assert isinstance(var, system.Pointer)
            if val == "self":
                assigned: system.Pointing = system.self_ptr
            elif val == "other":
                assigned = system.other_ptr
            elif val == "moddec":
                assigned = system.Moddec(var)
            elif val == "modinc":
                assigned = system.Modinc(var)
            else:
                _assigned = store.get_var(val)
                assert _assigned.var_type == system.VariableType.POINTER
                assert isinstance(_assigned, system.Pointer)
                assigned = _assigned
            pointer_assignments.append((var, assigned))
    return tuple(local_assignments), tuple(pointer_assignments)


def parse_iteration_exit(
        comp_interpreter: ComparisonTransformer,
        store: system.VariableStore,
        exit_json) -> system.IterationExit:
    comp_interpreter.local_ctx = system.other_ptr
    if (g := exit_json["guard"]) == "exceeded":
        guard: typing.Tuple[system.Comparison, ...] = tuple()
    else:
        guard = tuple(parse_comparison(comp_interpreter, expr)
                      for expr in g)
        assert guard
    comp_interpreter.local_ctx = None
    target = exit_json["target"]
    local_assg, ptr_assg = parse_assignment(
            store,
            exit_json.get("assignments", {}))
    return system.IterationExit(
            guard,
            target,
            local_assg,
            ptr_assg)

def parse_iteration_transition(
        comp_interpreter: ComparisonTransformer,
        store: system.VariableStore,
        transition_json,
        index: int) -> system.IteratingTransition:
    assert transition_json["type"] == "iteration"
    src = transition_json["src"]
    if "start" in transition_json:
        name = transition_json["start"]
        if name == "self":
            start: typing.Optional[system.Pointer] = system.self_ptr
        else:
            _start = store.get_var(name)
            assert isinstance(_start, system.Pointer)
            start = _start
    else:
        start = None
    if (it_dir := transition_json.get("direction", "up")) == "up":
        direction = system.IterationDirection.UP
    elif it_dir == "down":
        direction = system.IterationDirection.DOWN
    else:
        raise ValueError(f"Unknown iteration direction: {it_dir}")
    exits = tuple(
            parse_iteration_exit(comp_interpreter, store, exit)
            for exit in transition_json["exits"])
    return system.IteratingTransition(
            src,
            index,
            start,
            direction,
            exits)



def parse_local_transition(
        comp_interpreter: ComparisonTransformer,
        store: system.VariableStore,
        transition_json,
        index: int) -> system.LocalTransition:
    assert transition_json["type"] == "local"
    src = transition_json["src"]
    target = transition_json["target"]
    comp_interpreter.local_ctx = system.self_ptr
    guard = tuple(parse_comparison(comp_interpreter, expr)
                  for expr in transition_json.get("guard", []))
    comp_interpreter.local_ctx = None
    var_assg, ptr_assg = parse_assignment(
            store,
            transition_json.get("assignments", {}))
    return system.LocalTransition(
            src,
            index,
            target,
            guard,
            var_assg,
            ptr_assg)


def parse_state(store: system.VariableStore,
                state_json) -> system.AgentConfiguration:
    state: typing.List[typing.Tuple[system.LocalVariable, system.Value]] = []
    for name, val in state_json.items():
        var = store.get_var(name)
        assert isinstance(var, system.LocalVariable)
        value, = [v for v in var.values if v.name == val]
        state.append((var, value))
    return tuple(state)


def parse_property(store: system.VariableStore,
                   property_json) -> system.UnsafeProperty:
    name = property_json["name"]
    existentials = tuple(
            parse_state(store, ex_json)
            for ex_json in property_json["configuration"].get("existential",
                                                              []))
    universals = tuple(
            parse_state(store, univ_json)
            for univ_json in property_json["configuration"].get("universal",
                                                                []))
    return system.UnsafeProperty(
            name,
            existentials,
            universals)



def parse_system(system_json) -> system.System:
    # get states
    states = tuple(system_json["states"])
    state_var = parse_states_spec(states)
    # build store
    store = parse_variable_store(system_json["variables"], state_var)

    # create transformers
    comp_interpreter = ComparisonTransformer()
    comp_interpreter.store = store

    # extract remaining values
    transitions = tuple(
            parse_local_transition(comp_interpreter, store, t, i)
            if t["type"] == "local"
            else parse_iteration_transition(comp_interpreter, store, t, i)
            for i, t in enumerate(system_json["transitions"]))
    properties = tuple(parse_property(store, prop)
                       for prop in system_json["unsafe"])
    return system.System(store, tuple(transitions), properties)

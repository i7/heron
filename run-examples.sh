#!/bin/env bash

if [ $# -ne 2 ]
then
  echo "usage ${0} <log-dir> <result.csv>"
  exit 2
fi

PARENT=${1}
RESULT=${2}

mkdir ${PARENT} || { echo "Aborting..." ; exit 1; }

NAMES=(no-turn-dijkstra dijkstra knuth deBruijn eisenberg-mcguire)
LOGDIRS=()
for NAME in ${NAMES[@]}
do
  LOGFILE=${PARENT}/.log-${NAME}
  python main.py --log-dir ${LOGFILE} examples/${NAME}.json mutex
  LOGDIRS+=(${LOGFILE})
done

python aggregate-logs.py ${LOGDIRS[@]} ${RESULT}

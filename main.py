import argparse
import datetime
import pathlib
import sys
import json
import algorithm
import asyncio
import logging
import tempfile
import typing

from parser import parse_system

parser = argparse.ArgumentParser()

parser.add_argument("file",
                    action="store",
                    type=pathlib.Path,
                    help=("File name of specification of the analyzed "
                          "parameterized system."),
                    metavar="<file>")

parser.add_argument("prop",
                    action="store",
                    type=str,
                    help=("Name of the property which is checked."),
                    metavar="<prop>")

parser.add_argument("--prover-time",
                    action="store",
                    type=int,
                    help="Timeout (in seconds) for first-order proving steps.",
                    default=10,
                    metavar="<FO-t/o>s")

parser.add_argument("-v",
                    "--verbose",
                    action="count",
                    help=("Increases verbosity of output. This can be "
                          "specified multiple times."),
                    default=0)

parser.add_argument("-q",
                    "--quiet",
                    action="count",
                    help=("Decreases verbosity of output. This can be "
                          "specified multiple times."),
                    default=0)

parser.add_argument("--generalization-size",
                    action="store",
                    type=int,
                    help=("Overrides the automatically deduced generlization "
                          "size. Use with care since it may invalidate the "
                          "soundness of the proving process."),
                    metavar="<#trap>")

parser.add_argument("--log-dir",
                    action="store",
                    type=pathlib.Path,
                    help="Directory to write proved tptp problems and log.",
                    metavar="<log-dir>")

parser.add_argument("--instance-provers",
                    action="store",
                    type=int,
                    default=3,
                    help="Number of workers to prove finite instances.",
                    metavar="<#inst-worker>")

parser.add_argument("--fo-provers",
                    action="store",
                    type=int,
                    default=3,
                    help="Number of workers to prove general transitions.",
                    metavar="<#fo-worker>")

if __name__ == "__main__":
    args = parser.parse_args()
    if not args.file.exists():
        print(f"Cannot find file <{args.file}>")
        sys.exit(1)
    elif not args.file.is_file():
        print(f"Cannot open non-file <{args.file}>")
        sys.exit(1)
    try:
        with open(args.file, "r") as f:
            system_json = json.load(f)
        parsed_system = parse_system(system_json)
    except Exception as e:
        print(f"Failed to parse system in file <{args.file}>: {e}")
        sys.exit(1)
    if args.prop not in {prop.name for prop in parsed_system.properties}:
        print(f"Cannot find property <{args.prop}> in the parsed system.")
        sys.exit(1)
    verbosity = args.verbose - args.quiet
    if verbosity <= -2:
        logging.basicConfig(level=logging.CRITICAL)
    elif verbosity == -1:
        logging.basicConfig(level=logging.ERROR)
    elif verbosity == 0:
        logging.basicConfig(level=logging.WARNING)
    elif verbosity == 1:
        logging.basicConfig(level=logging.INFO)
    elif 2 <= verbosity:
        logging.basicConfig(level=logging.DEBUG)
    if args.generalization_size is not None:
        generalization_size = args.generalization_size
        print(f"Set generalization size to {generalization_size} per input.")
    else:
        generalization_size = parsed_system.generalization_size
        print(f"Set generalization size to {generalization_size} as safe "
              f"over-approximation.")
    config = algorithm.Config(
            args.file,
            args.prop,
            args.prover_time,
            verbosity,
            generalization_size,
            args.instance_provers,
            args.fo_provers)
    begin = datetime.datetime.now()
    check = asyncio.run(algorithm.main(config, parsed_system))
    duration = datetime.datetime.now() - begin
    if check.result:
        print(f"Successfully established <{config.prop}>!")
        if args.log_dir is not None:
            try:
                args.log_dir.mkdir(exist_ok=False)
            except FileExistsError:
                args.log_dir = None
        output_dir = args.log_dir
        if output_dir is None:
            output_dir = tempfile.mkdtemp(
                    prefix=f"proof-{args.file.stem}-",
                    suffix=f".log-dir",
                    dir=pathlib.Path.cwd())

        logs: typing.MutableMapping[str, typing.Any] = {
                "name": args.file.name,
                "time": f"{int(duration.total_seconds())}s",
                "obligations": []
                }

        for obligation in check.obligations:
            result: typing.MutableMapping[str, typing.Any] = {
                    "instances": [
                        {
                            "size": size+1,
                            "traps": sorted(
                                ("{"
                                    + ", ".join(sorted(p.pretty for p in trap))
                                    + "}")
                                for trap in traps)}
                        for size, traps in enumerate(obligation.instances)
                        if size != 0]}
            if isinstance(obligation, algorithm.LocalTransitionCheck):
                result["name"] = (f"local transition: "
                                  f"#{obligation.transition.index}")
                prefix = f"t-no.-{obligation.transition.index}-"
            elif isinstance(obligation, algorithm.IteratingTransitionBegin):
                result["name"] = (f"begin of iterating transition: "
                                  f"#{obligation.transition.index}")
                prefix = f"t-no.-{obligation.transition.index}-begin-"
            elif isinstance(obligation, algorithm.IteratingTransitionFirst):
                result["name"] = (f"first step of iterating transition: "
                                  f"#{obligation.transition.index}")
                prefix = f"t-no.-{obligation.transition.index}-first-"
            elif isinstance(obligation, algorithm.IteratingTransitionExit):
                result["name"] = (f"exit #{obligation.exit} of iterating "
                                  f"transition: #{obligation.transition.index}")
                prefix = (f"t-no.-{obligation.transition.index}-exit-no.-"
                          f"{obligation.exit}-")
            elif isinstance(obligation, algorithm.IteratingTransitionProgress):
                result["name"] = (f"progress guard "
                                  f"#{obligation.progress_guard} of iterating "
                                  f"transition: #{obligation.transition.index}")
                prefix = (f"t-no.-{obligation.transition.index}-progress-no.-"
                          f"{obligation.progress_guard}-")
            with tempfile.NamedTemporaryFile(mode="w",
                                             suffix=".p",
                                             prefix=prefix,
                                             dir=output_dir,
                                             delete=False) as tptp_file:
                print(algorithm.tptp_check(config, obligation),
                      flush=True,
                      file=tptp_file)
                result["proof"] = {
                        "invariants": [l.pretty
                                       for l in obligation.languages],
                        "tptp-problem": tptp_file.name}
            logs["obligations"].append(result)
        logfile = pathlib.Path(output_dir, "log.json")
        with logfile.open("w") as logfile_f:
            json.dump(logs, logfile_f, indent=2)
        print(f"Wrote logs into {output_dir}.")

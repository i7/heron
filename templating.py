import jinja2  # type: ignore
import system
import typing
import itertools
import words


TPTPFormula = typing.NewType("TPTPFormula", str)


def state_var_values(sys: system.System) -> typing.List[TPTPFormula]:
    res: typing.List[TPTPFormula] = []
    for state_val in sys.store.state_var.values:
        res.append(TPTPFormula(state_val.name))
        res += [TPTPFormula(f"{state_val.name}_iterating_{j}")
                for j in [transition.index
                          for transition in sys.transitions
                          if (isinstance(transition,
                                         system.IteratingTransition)
                              and transition.src == state_val.name)]]
    return res


def render_number(number: int) -> TPTPFormula:
    assert 0 <= number
    return TPTPFormula(number*"next(" + "zero" + ")"*number)


def render_comparison(comp: system.Comparison, subscript: str) -> TPTPFormula:
    if isinstance(comp.left, system.LocalTerm):
        typing_var: typing.Optional[system.LocalVariable] = comp.left.base
    elif isinstance(comp.right, system.LocalTerm):
        typing_var = comp.right.base
    else:
        typing_var = None
    if isinstance(comp.left, system.Pointer):
        left_term = comp.left.name
    elif isinstance(comp.left, system.LocalTerm):
        index_ptr = comp.left.index
        if index_ptr == system.self_ptr:
            index = "self"
        elif index_ptr == system.other_ptr:
            index = f"iteration_ptr_{subscript}(self)"
        else:
            index = index_ptr.name
        left_term = f"{comp.left.base.name}_{subscript}({index})"
    elif isinstance(comp.left, system.Value):
        assert typing_var is not None
        left_term = f"{typing_var.name}_{comp.left.name}"
    else:
        raise ValueError(f"Unexpexted left element in comparison: {comp.left}")
    if isinstance(comp.right, system.Pointer):
        right_term = comp.right.name
    elif isinstance(comp.right, system.LocalTerm):
        index_ptr = comp.right.index
        if index_ptr == system.self_ptr:
            index = "self"
        elif index_ptr == system.other_ptr:
            index = f"iteration_ptr_{subscript}(self)"
        else:
            index = index_ptr.name
        right_term = f"{comp.right.base.name}_{subscript}({index})"
    elif isinstance(comp.right, system.Value):
        assert typing_var is not None
        right_term = f"{typing_var.name}_{comp.right.name}"
    else:
        raise ValueError(f"Unexpexted right element in comparison: "
                         f"{comp.right}")
    if isinstance(comp, system.Less):
        return TPTPFormula(f"(leq({left_term}, {right_term}) "
                           f"& {left_term} != {right_term})")
    elif isinstance(comp, system.LessEqual):
        return TPTPFormula(f"leq({left_term}, {right_term})")
    elif isinstance(comp, system.Equal):
        return TPTPFormula(f"{left_term} = {right_term}")
    elif isinstance(comp, system.Unequal):
        return TPTPFormula(f"{left_term} != {right_term}")
    else:
        raise ValueError(f"Unexpected comparison: {comp}")


def render_ptr_assignment(assignment: typing.Tuple[system.Pointer,
                                                   system.Pointing]
                          ) -> TPTPFormula:
    ptr, pting = assignment
    assert ptr not in {system.self_ptr, system.other_ptr}
    if pting == system.self_ptr:
        return TPTPFormula(f"{ ptr.name }_after = self")
    elif pting == system.other_ptr:
        return TPTPFormula(f"{ ptr.name }_after = iteration_ptr_before(self)")
    elif isinstance(pting, system.Pointer):
        return TPTPFormula(f"{ ptr.name }_after = {pting.name}_before")
    elif isinstance(pting, system.Moddec):
        edge = (f"(({ pting.ptr.name }_before = next(zero)) "
                f"=> ({ ptr.name }_after = n))")
        reg = (f"((leq(next(next(zero)), { pting.ptr.name }_before)) "
               f"=> ({ ptr.name }_after = prev({ pting.ptr.name }_before)))")
        return TPTPFormula(f"{edge} & {reg}")
    elif isinstance(pting, system.Modinc):
        edge = (f"(({ pting.ptr.name }_before = n) "
                f"=> ({ ptr.name }_after = next(zero)))")
        reg = (f"((leq(next({ pting.ptr.name }_before), n)) "
               f"=> ({ ptr.name }_after = next({ pting.ptr.name }_before)))")
        return TPTPFormula(f"{edge} & {reg}")
    else:
        raise ValueError(f"Unexpected assignment {assignment}")


env = jinja2.Environment(
        undefined=jinja2.StrictUndefined,
        loader=jinja2.PackageLoader('heron'))


env.filters["state_var_values"] = state_var_values
env.filters["render_comparison"] = render_comparison
env.filters["render_number"] = render_number
env.filters["render_ptr_assignment"] = render_ptr_assignment


def fof_local_transition(sys: system.System,
                         local_transition: system.LocalTransition
                         ) -> TPTPFormula:
    theory = env.get_template("theory.fof").render(system=sys)
    change = env.get_template("local-transition.fof").render(
            transition=local_transition,
            system=sys)
    return TPTPFormula(f"{theory}\n\n{change}")


def fof_iterating_transition_begin(
        sys: system.System,
        iterating_transition: system.IteratingTransition
        ) -> TPTPFormula:
    theory = env.get_template("theory.fof").render(system=sys)
    begin = TPTPFormula(
            theory + "\n\n"
            + env.get_template("iterating-begin-transition.fof").render(
                system=sys,
                transition=iterating_transition,
                UPWARDS=system.IterationDirection.UP
                ))
    return begin


def fof_iterating_transition_first(
        sys: system.System,
        iterating_transition: system.IteratingTransition
        ) -> TPTPFormula:
    theory = env.get_template("theory.fof").render(system=sys)
    first = TPTPFormula(
            theory + "\n\n"
            + env.get_template("iterating-first-step-transition.fof").render(
                system=sys,
                transition=iterating_transition,
                UPWARDS=system.IterationDirection.UP
                ))
    return first


def fof_iterating_transition_exit(
        sys: system.System,
        iterating_transition: system.IteratingTransition,
        exit_index: int
        ) -> TPTPFormula:
    exit = iterating_transition.exits[exit_index]
    theory = env.get_template("theory.fof").render(system=sys)
    exit_formula = TPTPFormula(
        theory + "\n\n"
        + env.get_template("iterating-exit-transition.fof").render(
            system=sys,
            transition=iterating_transition,
            exit=exit,
            UPWARDS=system.IterationDirection.UP
            ))
    return exit_formula


def fof_iterating_transition_progress(
        sys: system.System,
        iterating_transition: system.IteratingTransition,
        guard_index: int
        ) -> TPTPFormula:
    theory = env.get_template("theory.fof").render(system=sys)
    progress = TPTPFormula(
        theory + "\n\n"
        + env.get_template("iterating-proceeding-transition.fof").render(
            system=sys,
            transition=iterating_transition,
            guard=iterating_transition.progress_guards[guard_index],
            UPWARDS=system.IterationDirection.UP
            ))
    return progress



def fof_property_inductive(
        prop: system.UnsafeProperty
        ) -> TPTPFormula:
    return TPTPFormula(
            env.get_template("property-inductive.fof").render(
                prop=prop))


def render_language(lang: words.Language, generalization_length: int):
    def _unbounded(rep: words.Repetition):
        return generalization_length <= rep.maximum
    def _star(rep: words.Repetition):
        return rep.minimum == 0 and _unbounded(rep)
    bounded_repetitions: typing.List[typing.Tuple[str,
                                                  words.Repetition,
                                                  str]] = []
    guards: typing.List[str] = []
    quantification_index = 0
    abstract_to_term: typing.MutableMapping[str, str] = {}
    current_term = "zero"
    lower_bound = "zero"
    upper_bound = f"Q{quantification_index}"
    quantification_index += 1
    for rep in lang.language:
        if rep.letter.abstract_index is not None:
            assert rep.minimum == 1 == rep.maximum
            abstract_to_term[rep.letter.abstract_index] = lower_bound
        if rep.minimum == rep.maximum and rep.maximum < generalization_length:
            advance = rep.minimum - 1
            advanced = advance*"next(" + lower_bound + ")"*advance
            bounded_repetitions.append((lower_bound, rep, advanced))
            lower_bound = f"next({advanced})"
        else:
            bounded_repetitions.append((lower_bound, rep, upper_bound))
            lower_bound = upper_bound
            upper_bound = f"Q{quantification_index}"
            quantification_index += 1
    if bounded_repetitions:
        lb, rep, ub = bounded_repetitions[-1]
        guards.append(f"{ub} = next(n)")
    quantified_vars: typing.Set[str] = set()
    for index in range(len(bounded_repetitions)):
        lb, rep, ub = bounded_repetitions[index]
        if lb.startswith("Q"):
            quantified_vars.add(lb)
        if ub.startswith("Q"):
            quantified_vars.add(ub)
        if rep.minimum != rep.maximum or generalization_length <= rep.maximum:
            if index < len(bounded_repetitions)-1 or rep.minimum == 0:
                # all but the last one are exclusive, but ending in a
                # potentially empty repetition can be treated non-inclusively
                min_dist = ("leq("
                            + rep.minimum*"next("
                            + lb
                            + ")"*rep.minimum
                            + ", "
                            + ub
                            + ")")
            else:
                # the last one is inclusive
                min_dist = ("leq("
                            + lb
                            + ", "
                            + (rep.minimum-1)*"next("
                            + ub
                            + ")"*(rep.minimum-1)
                            + ")")
            guards.append(min_dist)
            if not _unbounded(rep):
                diff = rep.maximum - rep.minimum
                if index < len(bounded_repetitions)-1:
                    max_dist = ("leq("
                                + ub
                                + ", "
                                + rep.maximum*"next("
                                + lb
                                + ")"*rep.maximum
                                + ")")
                else:
                    max_dist = ("leq("
                                + ub
                                + ", "
                                + (rep.maximum-1)*"next("
                                + lb
                                + ")"*(rep.maximum-1)
                                + ")")
                guards.append(max_dist)
    terms: typing.List[str] = []
    # care about iterations first:
    for abstract_rep in abstract_to_term:
        abstract_index = abstract_rep
        term = abstract_to_term[abstract_index]
        for t in lang.get_transitions_of(abstract_index):
            _terms: typing.List[str] = []
            slot = words.IterationSlot(abstract_index, t)
            def _get_stretch(start: int) -> typing.Tuple[int, int]:
                for beg in range(start, len(lang.language)+1):
                    if (beg < len(lang.language)
                        and slot in lang.language[beg].letter.letter):
                        break
                if beg == len(lang.language):
                    return beg, beg
                end = beg
                while (end < len(lang.language)
                       and slot in lang.language[end].letter.letter):
                    end += 1
                return beg, (end-1)
            end = -1
            while (s_indices := _get_stretch(end+1))[0] < len(lang.language):
                beg, end = s_indices
                beginning_term = bounded_repetitions[beg][0]
                ending_rep = (None
                              if len(lang.language) <= end
                              else bounded_repetitions[end])
                if ending_rep is None:
                    ending_term = "next(n)"
                else:
                    lb, rep, ub = ending_rep
                    if rep.minimum == rep.maximum:
                        ending_term = ub
                    elif end == len(lang.language)-1:
                        ending_term = "next(n)"
                    else:
                        ending_term = bounded_repetitions[end+1][0]
                ptr_term = f"iteration_ptr_{{temp}}({term})"
                _terms.append(
                        f"leq({beginning_term}, {ptr_term})"
                        f" & "
                        f"leq({ptr_term}, {ending_term})")
            terms.append(
                    f"state_{{temp}}({term}) "
                    + f"= state_{ t.src }_iterating_{ t.index }"
                    + f" & "
                    + "({bounds})".format(
                        bounds=" | ".join(f"({t})" for t in _terms)))

    # pointers second:
    for ptr in {p.ptr
                for rep in lang.language
                for p in rep.letter.letter
                if isinstance(p, words.PointerSlot)}:
        _terms = []
        ptr_slot = words.PointerSlot(ptr)
        def _get_stretch(start: int) -> typing.Tuple[int, int]:
            for beg in range(start, len(lang.language)+1):
                if (beg < len(lang.language)
                    and ptr_slot in lang.language[beg].letter.letter):
                    break
            if beg == len(lang.language):
                return beg, beg
            end = beg
            while (end < len(lang.language)
                   and ptr_slot in lang.language[end].letter.letter):
                end += 1
            return beg, (end-1)
        end = -1
        while (s_indices := _get_stretch(end+1))[0] < len(lang.language):
            beg, end = s_indices
            beginning_term = bounded_repetitions[beg][0]
            ending_rep = (None
                          if len(lang.language) <= end
                          else bounded_repetitions[end])
            if ending_rep is None:
                ending_term = "next(n)"
            else:
                lb, rep, ub = ending_rep
                if rep.minimum == rep.maximum:
                    ending_term = ub
                elif end == len(lang.language)-1:
                    ending_term = "next(n)"
                else:
                    ending_term = bounded_repetitions[end+1][0]
            ptr_term = f"{ptr.name}_{{temp}}"
            _terms.append(
                    f"leq({beginning_term}, {ptr_term})"
                    f" & "
                    f"leq({ptr_term}, {ending_term})")
        terms.append("({bounds})".format(
                     bounds=" | ".join(f"({t})" for t in _terms)))

    # local variables third:
    for lb, rep, ub in bounded_repetitions:
        if rep.minimum == rep.maximum:
            current_term = lb
            for _ in range(rep.minimum):
                for p in rep.letter.letter:
                    if isinstance(p, words.LocalVariableSlot):
                        terms.append(
                                f"{p.variable.name}_{{temp}}({current_term})"
                                f" = "
                                f"{p.variable.name}_{p.value.name}")
                current_term = f"next({current_term})"
        else:
            _terms = []
            for p in rep.letter.letter:
                if isinstance(p, words.LocalVariableSlot):
                    _terms.append(
                        f"{p.variable.name}_{{temp}}(B)"
                        f" = "
                        f"{p.variable.name}_{p.value.name}")
            guard = (f"leq({lb}, B)"
                     f" & "
                     f"leq(next(B), {ub})")
            if _terms:
                terms.append(
                        f"?[B]: (({guard}) & (" + " | ".join(_terms) + "))")
    res = ("![" + ", ".join(sorted(quantified_vars)) + "]: (("
           + " & ".join(guards) + ") => ({formula}))".format(
               formula=" | ".join(f"({t})" for t in terms)))
    return (TPTPFormula(res.format(temp="before")),
            TPTPFormula(res.format(temp="after")))

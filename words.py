import typing
import dataclasses
import system
import instance
import types


class IterationSlot(typing.NamedTuple):
    agent: str
    transition: system.IteratingTransition

    @property
    def pretty(self) -> str:
        return f"t_{self.transition.index}[{self.agent}]"


class LocalVariableSlot(typing.NamedTuple):
    variable: system.LocalVariable
    value: system.Value

    @property
    def pretty(self) -> str:
        return f"{self.variable.name} = {self.value.name}"


class PointerSlot(typing.NamedTuple):
    ptr: system.Pointer

    @property
    def pretty(self) -> str:
        return self.ptr.name


Letter = typing.FrozenSet[typing.Union[IterationSlot,
                                       LocalVariableSlot,
                                       PointerSlot]]


class AbstractLetter(typing.NamedTuple):
    letter: Letter
    abstract_index: typing.Optional[str]  # not abstract if `None`

    @property
    def pretty(self) -> str:
        return ((""
                 if self.abstract_index is None
                 else f"{self.abstract_index}:")
                + "{" + ", ".join(sorted(p.pretty for p in self.letter)) + "}")


@dataclasses.dataclass(frozen=True)
class Word:
    letters: typing.Tuple[AbstractLetter, ...]

    @classmethod
    def from_place_set(cls,
                       place_set: typing.FrozenSet[
                           typing.Union[instance.PointerPlace,
                                        instance.IterationPlace,
                                        instance.LocalVariablePlace]],
                       size: int) -> "Word":

        iteration_places = frozenset({
                               p for p in place_set
                               if isinstance(p, instance.IterationPlace)})
        local_variable_places = frozenset({
                               p for p in place_set
                               if isinstance(p, instance.LocalVariablePlace)})
        pointer_places = frozenset({
                               p for p in place_set
                               if isinstance(p, instance.PointerPlace)})

        abstract_indices = {
                i: f"P{pos}"
                for pos, i in enumerate(sorted(
                    {p.agent for p in iteration_places}))}
        letters: typing.List[AbstractLetter] = []
        for i in range(size+2):
            letters.append(
                    AbstractLetter(
                        frozenset(
                            {
                                IterationSlot(
                                    abstract_indices[p.agent],
                                    p.transition)
                                for p in iteration_places
                                if p.value == i-1}
                            | {
                                PointerSlot(p.pointer)
                                for p in pointer_places
                                if p.value == i-1}
                            | {
                                LocalVariableSlot(
                                    p.variable,
                                    p.value)
                                for p in local_variable_places
                                if p.agent == i-1}),
                    abstract_indices.get(i-1, None)))
        return cls(tuple(letters))


@dataclasses.dataclass(frozen=True)
class Repetition:
    letter: AbstractLetter
    minimum: int
    maximum: int

    def __post_init__(self):
        object.__setattr__(self,
                           "_iteration_slots",
                           frozenset({
                               p for p in self.letter.letter
                               if isinstance(p, IterationSlot)}))

    @property
    def iteration_slots(self) -> typing.FrozenSet[IterationSlot]:
        return self._iteration_slots  # type: ignore

    @property
    def pretty(self) -> str:
        return f"{self.letter.pretty}[{self.minimum}, {self.maximum}]"


@dataclasses.dataclass(frozen=True)
class Language:
    language: typing.Tuple[Repetition, ...]

    @classmethod
    def from_word(cls, word: Word) -> "Language":
        language: typing.List[Repetition] = []
        letters = word.letters
        view_start = 0
        while view_start < len(letters):
            letter = letters[view_start]
            length = 1
            while (view_start + length < len(letters)
                   and letters[view_start + length] == letter):
                length += 1
            language.append(
                    Repetition(
                        letter,
                        length,
                        length))
            view_start += length
        return cls(tuple(language))

    def __post_init__(self):
        transitions_of_abstracts: typing.MutableMapping[
                str, typing.Set[system.IteratingTransition]] = {}
        for rep in self.language:
            for it_slot in rep.iteration_slots:
                if it_slot.agent not in transitions_of_abstracts:
                    transitions_of_abstracts[it_slot.agent] = set()
                transitions_of_abstracts[it_slot.agent].add(it_slot.transition)
        object.__setattr__(self,
                           "_transitions_of_abstracts",
                           types.MappingProxyType({
                               k: frozenset(val)
                               for k, val in transitions_of_abstracts.items()})
                           )

    def get_transitions_of(self,
                           abstract_index: str
                           ) -> typing.FrozenSet[system.IteratingTransition]:
        return self._transitions_of_abstracts.get(abstract_index, frozenset())  # type: ignore

    @property
    def pretty(self) -> str:
        return "".join(rep.pretty for rep in self.language)

    def cap_repetitions(self, where: int) -> "Language":
        repetitions: typing.List[Repetition] = []
        for rep in self.language:
            repetitions.append(
                    Repetition(
                        rep.letter,
                        min(rep.minimum, where),
                        min(rep.maximum, where)))
        return Language(tuple(repetitions))

    def contains(self, other: "Language") -> bool:
        """
        Checks if self contains other.
        """
        other_index = 0
        for rep in self.language:
            if len(other.language) < other_index:
                return False
            elif len(other.language) == other_index:
                if rep.minimum != 0:
                    return False
                else:
                    continue
            other_rep = other.language[other_index]
            if rep.letter != other_rep.letter:
                if rep.minimum != 0:
                    return False
                else:
                    continue
            if rep.letter == other_rep.letter:
                if not (rep.minimum <= other_rep.minimum
                        and other_rep.maximum <= rep.maximum):
                    return False
                else:
                    other_index += 1
        return other_index == len(other.language)

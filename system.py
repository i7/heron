import typing
import itertools
import dataclasses
import enum
import collections
import types


class VariableType(enum.Enum):
    LOCAL = "LOCAL"
    POINTER = "POINTER"
    INDEXED = "INDEXED"


@dataclasses.dataclass(frozen=True)
class Term:
    pass


@dataclasses.dataclass(frozen=True)
class Value(Term):
    name: str
    position: int

    @property
    def pretty(self):
        return self.name


@dataclasses.dataclass(frozen=True)
class Pointer(Term):
    name: str

    @property
    def var_type(self) -> VariableType:
        return VariableType.POINTER

    @property
    def pretty(self):
        return f"*{self.name}"


@dataclasses.dataclass(frozen=True)
class Moddec(Term):
    ptr: Pointer

    @property
    def var_type(self) -> VariableType:
        return VariableType.POINTER

    @property
    def pretty(self):
        return f"({self.ptr.pretty} - 1 mod size)"


@dataclasses.dataclass(frozen=True)
class Modinc(Term):
    ptr: Pointer

    @property
    def var_type(self) -> VariableType:
        return VariableType.POINTER

    @property
    def pretty(self):
        return f"(*{self.ptr.pretty} + 1 mod size)"


Pointing = typing.Union[Pointer, Moddec, Modinc]
def is_pointing(obj) -> bool:
    return (isinstance(obj, Pointer)
            or isinstance(obj, Moddec)
            or isinstance(obj, Modinc))


self_ptr = Pointer("self")
other_ptr = Pointer("other")


ReservedPointer = frozenset({
    self_ptr,
    other_ptr,
    })


@dataclasses.dataclass(frozen=True)
class LocalVariable(Term):
    name: str
    values: typing.Tuple[Value, ...]

    @property
    def var_type(self) -> VariableType:
        return VariableType.LOCAL

    @property
    def pretty(self):
        return f"{self.name}"


ReservedVariableNames = frozenset({
        "state",
        })


@dataclasses.dataclass(frozen=True)
class LocalTerm(Term):
    base: LocalVariable
    index: Pointer

    @property
    def var_type(self) -> VariableType:
        return VariableType.INDEXED

    @property
    def pretty(self):
        return f"{self.base.pretty}[{self.index.pretty}]"


@dataclasses.dataclass(frozen=True)
class VariableStore:
    pointers: typing.Tuple[Pointer, ...]
    local_vars: typing.Tuple[LocalVariable, ...]
    state_var: LocalVariable

    def __post_init__(self):
        object.__setattr__(self,
                           "_name_lookup",
                           types.MappingProxyType(
                               {t.name: t
                                for t in self.pointers + self.local_vars}))

    def get_var(self, name: str) -> typing.Union[Pointer, LocalVariable]:
        return self._name_lookup[name]  # type: ignore


LocalAssignment = typing.Tuple[LocalTerm, Value]
PointerAssignment = typing.Tuple[Pointer, int]


@dataclasses.dataclass(frozen=True)
class Interpretation:
    local_assignments: typing.Tuple[LocalAssignment, ...]
    pointer_assignments: typing.Tuple[PointerAssignment, ...]

    def __post_init__(self):
        object.__setattr__(
                self,
                "_local_assignments",
                types.MappingProxyType(
                    {lterm: val for lterm, val in self.local_assignments}))
        object.__setattr__(
                self,
                "_pointer_assignments",
                types.MappingProxyType(
                    {ptr: val for ptr, val in self.pointer_assignments}))

    @property
    def map_local(self):
        return self._local_assignments  # type: ignore

    @property
    def map_ptr(self):
        return self._pointer_assignments  # type: ignore


@dataclasses.dataclass(frozen=True)
class Comparison:
    left: typing.Union[Pointer, LocalTerm, Value]
    right: typing.Union[Pointer, LocalTerm, Value]

    def evaluate(self, interpretation: Interpretation) -> bool:
        raise NotImplementedError()

    @property
    def pretty(self) -> str:
        raise NotImplementedError()

    @property
    def negation(self) -> "Comparison":
        raise NotImplementedError()


@dataclasses.dataclass(frozen=True)
class Equal(Comparison):
    @property
    def pretty(self) -> str:
        return f"{self.left.pretty} == {self.right.pretty}"

    @property
    def negation(self) -> "Unequal":
        return Unequal(self.left, self.right)

    def evaluate(self, interpretation: Interpretation) -> bool:
        if isinstance(self.left, Pointer):
            assert self.left in interpretation.map_ptr
            assert isinstance(self.right, Pointer)
            assert self.right in interpretation.map_ptr
            return (interpretation.map_ptr[self.left]
                    == interpretation.map_ptr[self.right])
        elif isinstance(self.left, LocalTerm):
            assert self.left in interpretation.map_local
            assert isinstance(self.right, Value)
            return interpretation.map_local[self.left] == self.right
        else:
            raise ValueError(f"Unexpected type <{type(self.left)}> in comp.")


@dataclasses.dataclass(frozen=True)
class Unequal(Comparison):
    @property
    def pretty(self) -> str:
        return f"{self.left.pretty} != {self.right.pretty}"

    @property
    def negation(self) -> Equal:
        return Equal(self.left, self.right)

    def evaluate(self, interpretation: Interpretation) -> bool:
        if isinstance(self.left, Pointer):
            assert self.left in interpretation.map_ptr
            assert isinstance(self.right, Pointer)
            assert self.right in interpretation.map_ptr
            return (interpretation.map_ptr[self.left]
                    != interpretation.map_ptr[self.right])
        elif isinstance(self.left, LocalTerm):
            assert self.left in interpretation.map_local
            assert isinstance(self.right, Value)
            return interpretation.map_local[self.left] != self.right
        else:
            raise ValueError(f"Unexpected type <{type(self.left)}> in comp.")

@dataclasses.dataclass(frozen=True)
class Less(Comparison):
    @property
    def pretty(self) -> str:
        return f"{self.left.pretty} < {self.right.pretty}"

    @property
    def negation(self) -> "LessEqual":
        return LessEqual(self.right, self.left)

    def evaluate(self, interpretation: Interpretation) -> bool:
        if isinstance(self.left, Pointer):
            assert self.left in interpretation.map_ptr
            assert isinstance(self.right, Pointer)
            assert self.right in interpretation.map_ptr
            return (interpretation.map_ptr[self.left]
                    < interpretation.map_ptr[self.right])
        elif isinstance(self.left, LocalTerm):
            assert self.left in interpretation.map_local
            assert isinstance(self.right, Value)
            return (interpretation.map_local[self.left].position
                    < self.right.position)
        else:
            raise ValueError(f"Unexpected type <{type(self.left)}> in comp.")

@dataclasses.dataclass(frozen=True)
class LessEqual(Comparison):
    @property
    def pretty(self) -> str:
        return f"{self.left.pretty} <= {self.right.pretty}"

    @property
    def negation(self) -> Less:
        return Less(self.right, self.left)

    def evaluate(self, interpretation: Interpretation) -> bool:
        if isinstance(self.left, Pointer):
            assert self.left in interpretation.map_ptr
            assert isinstance(self.right, Pointer)
            assert self.right in interpretation.map_ptr
            return (interpretation.map_ptr[self.left]
                    <= interpretation.map_ptr[self.right])
        elif isinstance(self.left, LocalTerm):
            assert self.left in interpretation.map_local
            assert isinstance(self.right, Value)
            return (interpretation.map_local[self.left].position
                    <= self.right.position)
        else:
            raise ValueError(f"Unexpected type <{type(self.left)}> in comp.")


class TransitionType(enum.Enum):
    LOCAL = "LOCAL"
    ITERATION = "ITERATION"


class IterationDirection(enum.Enum):
    UP = "UP"
    DOWN = "DOWN"


@dataclasses.dataclass(frozen=True)
class Transition:
    src: str
    index: int

    @property
    def maximal_involved_indices(self) -> int:
        raise NotImplementedError()

    @property
    def transition_type(self) -> TransitionType:
        raise NotImplementedError()

    def pretty(self) -> str:
        raise NotImplementedError()


@dataclasses.dataclass(frozen=True)
class LocalTransition(Transition):
    target: str
    guard: typing.Tuple[Comparison, ...]
    variable_assignments: typing.Tuple[typing.Tuple[LocalVariable, Value], ...]
    pointer_assignments: typing.Tuple[typing.Tuple[Pointer, Pointing], ...]

    def __post_init__(self):
        object.__setattr__(
                self,
                "_var_map",
                types.MappingProxyType(
                    {var: val for var, val in self.variable_assignments}))
        object.__setattr__(
                self,
                "_ptr_map",
                types.MappingProxyType(
                    {lptr: rptr for lptr, rptr in self.pointer_assignments}))


    @property
    def maximal_involved_indices(self) -> int:
        pointing_elements: typing.Set[Pointing] = {self_ptr}
        for lptr, rptr in self.pointer_assignments:
            pointing_elements.add(lptr)
            pointing_elements.add(rptr)
        for comp in self.guard:
            if isinstance(comp.left, Pointer):
                pointing_elements.add(comp.left)
            elif isinstance(comp.left, LocalTerm):
                pointing_elements.add(comp.left.index)
            if isinstance(comp.right, Pointer):
                pointing_elements.add(comp.right)
            elif isinstance(comp.right, LocalTerm):
                pointing_elements.add(comp.right.index)
        return len(pointing_elements)

    @property
    def var_map(self) -> typing.Mapping[LocalVariable, Value]:
        return self._var_map  # type: ignore

    @property
    def ptr_map(self) -> typing.Mapping[Pointer, Pointer]:
        return self._ptr_map  # type: ignore

    @property
    def pretty(self) -> str:
        if not self.guard:
            guard = "true"
        else:
            guard = " & ".join(comp.pretty for comp in self.guard)
        assignments = ", ".join(
                [f"{var.pretty}: {val.name}"
                 for var, val in self.variable_assignments]
                + [f"{lptr.pretty}: {rptr.pretty}"
                   for lptr, rptr in self.pointer_assignments])
        return f"{self.src} -- [{guard}]{{{assignments}}} --> {self.target}"


@dataclasses.dataclass(frozen=True)
class IterationExit:
    guard: typing.Tuple[Comparison, ...]
    target: str
    variable_assignments: typing.Tuple[typing.Tuple[LocalVariable, Value], ...]
    pointer_assignments: typing.Tuple[typing.Tuple[Pointer, Pointing], ...]

    def __post_init__(self):
        object.__setattr__(
                self,
                "_var_map",
                types.MappingProxyType(
                    {var: val for var, val in self.variable_assignments}))
        object.__setattr__(
                self,
                "_ptr_map",
                types.MappingProxyType(
                    {lptr: rptr for lptr, rptr in self.pointer_assignments}))

    @property
    def var_map(self) -> typing.Mapping[LocalVariable, Value]:
        return self._var_map  # type: ignore

    @property
    def ptr_map(self) -> typing.Mapping[Pointer, Pointer]:
        return self._ptr_map  # type: ignore

    @property
    def pretty(self) -> str:
        if not self.guard:
            guard = "exceeded"
        else:
            guard = " & ".join(comp.pretty for comp in self.guard)
        assignments = ", ".join(
                [f"{var.pretty}: {val.name}"
                 for var, val in self.variable_assignments]
                + [f"{lptr.pretty}: {rptr.pretty}"
                   for lptr, rptr in self.pointer_assignments])
        return f"exit -- [{guard}]{{{assignments}}} --> {self.target}"


@dataclasses.dataclass(frozen=True)
class IteratingTransition(Transition):
    start: typing.Optional[Pointer]
    direction: IterationDirection
    exits: typing.Tuple[IterationExit, ...]

    def __post_init__(self):
        object.__setattr__(self,
                           "_progress_guard",
                           tuple(itertools.product(*([comp.negation
                                                      for comp in exit.guard]
                                                     for exit in self.exits
                                                     if exit.guard))))

    @property
    def maximal_involved_indices(self) -> int:
        pointing_elements: typing.Set[Pointing] = {self_ptr, other_ptr}
        if self.start is not None:
            pointing_elements.add(self.start)
        def _ptrs_for_exit(exit: IterationExit) -> typing.Set[Pointing]:
            pointing_elements: typing.Set[Pointing] = {self_ptr, other_ptr}
            for comp in exit.guard:
                if isinstance(comp.left, Pointer):
                    pointing_elements.add(comp.left)
                elif isinstance(comp.left, LocalTerm):
                    pointing_elements.add(comp.left.index)
                if isinstance(comp.right, Pointer):
                    pointing_elements.add(comp.right)
                elif isinstance(comp.right, LocalTerm):
                    pointing_elements.add(comp.right.index)
            for lptr, rptr in exit.pointer_assignments:
                pointing_elements.add(lptr)
                pointing_elements.add(rptr)
            return pointing_elements
        for exit in self.exits:
            pointing_elements |= _ptrs_for_exit(exit)
        return len(pointing_elements) + 1 # adding 1 for other+1 on advancement

    @property
    def progress_guards(self) -> typing.Tuple[typing.Tuple[Comparison]]:
        return self._progress_guard  # type: ignore

    @property
    def pretty(self) -> str:
        if self.start:
            start = self.start.name
        else:
            start = "⊥" if self.direction == IterationDirection.UP else "⊤"
        direction = "↑" if self.direction == IterationDirection.UP else "↓"
        exits = " || ".join(exit.pretty for exit in self.exits)
        return f"{self.src}: {start}[{direction}] >> {exits}"


AgentConfiguration = typing.Tuple[typing.Tuple[LocalVariable, Value], ...]
ConfigurationCollection = typing.Tuple[AgentConfiguration, ...]


@dataclasses.dataclass(frozen=True)
class UnsafeProperty:
    name: str
    existentials: ConfigurationCollection
    inbetweens: ConfigurationCollection

    def __post_init__(self):
        if not (len(self.inbetweens) == len(self.existentials)+1
                or (not self.inbetweens and self.existentials)):
            raise ValueError(f"State of property {self.name} inconsistent")
        for conf in self.existentials + self.inbetweens:
            variables: typing.Set[system.Variable] = set()
            for var, val in conf:
                if not val in var.values:
                    raise ValueError(f"Cannot assign value {val} to {var}")
                if var in variables:
                    raise ValueError(f"Variable {var} is assigned "
                                     f"multiple times in {conf}")


@dataclasses.dataclass(frozen=True)
class System:
    store: VariableStore
    transitions: typing.Tuple[Transition, ...]
    properties: typing.Tuple[UnsafeProperty, ...]

    @property
    def generalization_size(self) -> int:
        return max(t.maximal_involved_indices for t in self.transitions)

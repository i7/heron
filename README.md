# heron

`heron` is a tool for the analysis of parameterized systems with non-atomic
global checks. The principle under which heron operates is as follows:

1) instantiate the parameterized system for an index `N`,
2) check whether this instance can be proven via traps,
3) soundly generalize traps of instances to families of invariants,
4) check via first-order theorem proving whether the considered families of
   invariants suffice to prove inductiveness of the desired property.

## Requirements

`heron` relies on the following dependencies:

- python package `jinja2`
- python package `lark-parser`
- python package [`processnet`](https://gitlab.lrz.de/i7/processnet)
- tool `cvc4` in path
- tool `vampire` in path
- recent version of python; i.e., `Python 3.9.2` or above

## Examples

`./examples` contains examples of mutual exclusion algorithms. `./.logs`
contains logs for a run of `heron` on these examples.

## Contact

This tool is maintained by Christoph Welzel (`welzel@in.tum.de`).

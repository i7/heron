import system
import typing
from typing import List, FrozenSet
import dataclasses
import instance
import templating
import asyncio
import tempfile
import logging
import words
import pathlib
from itertools import product, permutations

from processnet import net as pn  # type: ignore
from processnet import algorithm as pn_alg  # type: ignore


fo_logger = logging.getLogger("fo-logging")
instance_logger = logging.getLogger("instance-logging")
parameterized_logger = logging.getLogger("parameterized-logger")


InstanceStore = typing.MutableMapping[int, instance.Instance]


@dataclasses.dataclass(frozen=True)
class Config:
    system_file: pathlib.Path
    """
    Path to file which is analyzed.
    """

    prop: str
    """
    Name of the property which is checked.
    """

    prover_timeout: int
    """
    Time to wait for FO provers to establish the desired result.
    """

    verbosity: int
    """
    Value to indicate how much output is generated.
    """

    generalization_size: int
    """
    Generalization size for the desired system.
    """

    instance_provers: int
    """
    Amount of tasks which prove instances of transitions.
    """

    fo_provers: int
    """
    Amount of tasks which prove the general transitions.
    """


class UNSATError(Exception):
    pass


def is_trap(trap: pn.PlaceSet, inst: instance.Instance) -> bool:
    if not trap <= inst.initial_marking.net.places:
        return False
    for pre, post in inst.initial_marking.net.transitions:
        if pre & trap and not post & trap:
            return False
    return True


def language_traps(
        lang: words.Language,
        generalization_size: int,
        length: int) -> typing.FrozenSet[pn.PlaceSet]:
    """
    Returns all traps encoded in all words of `lang` of length `length` if we
    allow adding repetitions for `generalization_size` big blocks.
    """
    ranges = [range(rep.minimum,
                    rep.maximum+1
                    if rep.maximum < generalization_size
                    else length+1)
            for rep in lang.language]
    result: typing.Set[pn.PlaceSet] = set()
    for choice in [choice
                   for choice in product(*ranges)
                   if sum(choice) == length]:
        abstract_indices: typing.MutableMapping[str, int] = {}
        current_index = -1
        for rep, amount in zip(lang.language, choice):
            if rep.letter.abstract_index is not None:
                assert amount == 1
                abstract_indices[rep.letter.abstract_index] = current_index
                current_index += 1
            else:
                current_index += amount
        current_index = -1
        current_trap: typing.Set[pn.Place] = set()
        for rep, amount in zip(lang.language, choice):
            for step in range(amount):
                assert rep.minimum <= amount
                if rep.maximum < generalization_size:
                    assert amount <= rep.maximum
                else:
                    assert amount <= length
                for p in rep.letter.letter:
                    if isinstance(p, words.IterationSlot):
                        current_trap.add(
                                instance.IterationPlace(
                                    p.transition,
                                    abstract_indices[p.agent],
                                    current_index))
                    elif isinstance(p, words.LocalVariableSlot):
                        current_trap.add(
                                instance.LocalVariablePlace(
                                    p.variable,
                                    p.value,
                                    current_index))
                    elif isinstance(p, words.PointerSlot):
                        current_trap.add(
                                instance.PointerPlace(
                                    p.ptr,
                                    current_index))
                    else:
                        raise ValueError(f"Unexpected slot <{type(p)}>")
                current_index += 1
        result.add(frozenset(current_trap))
    return frozenset(result)


def all_finite_language_traps(
        lang: words.Language
        ) -> typing.FrozenSet[typing.Tuple[int, pn.PlaceSet]]:
    """
    Returns all actual instances of lang with the corresponding length of the
    word.
    """
    maximum_repetition = max(rep.maximum for rep in lang.language)
    minimal_length = sum(rep.minimum for rep in lang.language)
    maximal_length = sum(rep.maximum for rep in lang.language)
    result: typing.Set[typing.Tuple[int, pn.PlaceSet]] = set()
    for length in range(minimal_length, maximal_length+1):
        result |= {
                (length, t)
                for t in language_traps(lang, maximum_repetition+1, length)}
    return frozenset(result)


async def prove(formula: templating.TPTPFormula, timeout_s: int = 20) -> bool:
    cmds = [
            ["vampire", "--mode portfolio", "--statistics none", "--proof off", f"-t {timeout_s}s"],
            ["cvc4", "-q", "--lang=tptp", f"--tlimit={1000*timeout_s}", "-"],
           ]
    fo_provers = []
    for cmd in cmds:
        fo_logger.debug(f"Creating process with cmd: {cmd}")
        fo_provers.append(
                await asyncio.create_subprocess_shell(
                    " ".join(cmd),
                    stdin=asyncio.subprocess.PIPE,
                    stderr=asyncio.subprocess.PIPE,
                    stdout=asyncio.subprocess.PIPE))
    pending = {asyncio.create_task(fo_prover.communicate(formula.encode()))
               for fo_prover in fo_provers}
    while pending:
        done, pending = await asyncio.wait(pending,
                                           return_when=asyncio.FIRST_COMPLETED)
        for res in done:
            try:
                result = res.result()
            except asyncio.CancelledError:
                continue
            out, err = result[0].decode(), result[1].decode()
            if err:
                fo_logger.warning(f"FO error:{err}\nfor formula\n:{formula}")
            fo_logger.debug(f"Result:\n{out}\nInput:{formula}")
            if "SZS status Theorem" in out:
                for future in pending:
                    future.cancel()
                return True
    return False


async def unreach(initial_marking: pn.Marking,
                  bad_markings: typing.Iterable[pn.Marking],
                  transitions: typing.Iterable[pn.Transition],
                  initial_traps: typing.Collection[pn.PlaceSet],
                  flows: typing.Collection[pn.PlaceSet]
                  ) -> typing.Optional[typing.FrozenSet[pn.PlaceSet]]:
    """
    Check inductiveness of the fact that `bad_markings` cannot be covered for
    `transitions` if `initial_marking` is the initial marking and we assume
    `flows` to be valid flows`.
    """
    traps: typing.Set[pn.PlaceSet] = set()
    initial_trap_set = set(initial_traps)

    for cover in bad_markings:
        for t in transitions:
            while (ce := await asyncio.to_thread(
                    pn_alg.get_counter_example,
                    initial_marking.net,
                    t,
                    cover,
                    pn_alg.Invariants(traps | initial_trap_set, flows, set()))):
                if (trap := await asyncio.to_thread(
                        pn_alg.find_trap,
                        initial_marking,
                        ce)):
                    traps.add(trap)
                    continue
                else:
                    return None
    return frozenset(traps)


@dataclasses.dataclass
class Obligation:
    formula: templating.TPTPFormula
    discharged: bool = dataclasses.field(init=False, default=False)
    instances: List[FrozenSet[pn.PlaceSet]] = dataclasses.field(
            init=False,
            default_factory=lambda: [frozenset()])
    languages: typing.Set[words.Language] = dataclasses.field(
            init=False,
            default_factory=set)
    attempts: int = dataclasses.field(
            init=False,
            default=0)


    def add_languages(self, languages: typing.Iterable[words.Language]):
        new_languages = {l for l in languages
                         if not any(old_l.contains(l)
                                    for old_l in self.languages)}
        minimized = {l for l in self.languages
                     if not any(new_l.contains(l)
                                for new_l in new_languages)}
        self.languages = minimized | new_languages


@dataclasses.dataclass
class LocalTransitionCheck(Obligation):
    transition: system.LocalTransition


@dataclasses.dataclass
class IteratingTransitionBegin(Obligation):
    transition: system.IteratingTransition


@dataclasses.dataclass
class IteratingTransitionFirst(Obligation):
    transition: system.IteratingTransition


@dataclasses.dataclass
class IteratingTransitionExit(Obligation):
    transition: system.IteratingTransition
    exit: int


@dataclasses.dataclass
class IteratingTransitionProgress(Obligation):
    transition: system.IteratingTransition
    progress_guard: int



def tptp_check(
        config: Config,
        obligation: Obligation) -> templating.TPTPFormula:
    invariants = [
            ("fof(min_size, axiom, "
             "leq("
             + templating.render_number(len(obligation.instances)+1)
             +", n)).")]
    for i, lang in enumerate(obligation.languages):
        bef, aft = templating.render_language(
                lang,
                config.generalization_size)
        invariants += [
            f"% {lang.pretty}",
            f"fof(inv_{i}_before, axiom, {bef}).",
            f"fof(inv_{i}_after, axiom, {aft})."]
    check = "{transition}\n{invariants}".format(
            transition=obligation.formula,
            invariants="\n".join(invariants))
    return templating.TPTPFormula(check)


@dataclasses.dataclass
class Check:
    config: Config
    sys: system.System
    prop: system.UnsafeProperty

    instance_store: InstanceStore = dataclasses.field(
            init=False,
            default_factory=dict)
    obligations: typing.List[Obligation] = dataclasses.field(
            init=False,
            default_factory=list)
    result: bool = dataclasses.field(
            init=False,
            default=False)


    def __post_init__(self):
        inductiveness = templating.fof_property_inductive(self.prop)
        for t in self.sys.transitions:
            print(f"Adding transition #{t.index}")
            if isinstance(t, system.LocalTransition):
                self.obligations.append(LocalTransitionCheck(
                    templating.TPTPFormula(
                        templating.fof_local_transition(self.sys, t)
                        + "\n"
                        + inductiveness),
                    t))
            elif isinstance(t, system.IteratingTransition):
                self.obligations += (
                        [
                            IteratingTransitionBegin(
                                templating.TPTPFormula(
                                    templating.fof_iterating_transition_begin(
                                        self.sys,
                                        t)
                                    + "\n"
                                    + inductiveness),
                                t),
                            IteratingTransitionFirst(
                                templating.TPTPFormula(
                                    templating.fof_iterating_transition_first(
                                        self.sys,
                                        t)
                                    + "\n"
                                    + inductiveness),
                                t)]
                        + [
                            IteratingTransitionExit(
                                templating.TPTPFormula(
                                    templating.fof_iterating_transition_exit(
                                        self.sys,
                                        t,
                                        e)
                                    + "\n"
                                    + inductiveness),
                                t,
                                e)
                            for e in range(len(t.exits))]
                        + [
                            IteratingTransitionProgress(
                                templating.TPTPFormula(
                                    templating.fof_iterating_transition_progress(
                                        self.sys,
                                        t,
                                        p)
                                    + "\n"
                                    + inductiveness),
                                t,
                                p)
                            for p in range(len(t.progress_guards))])
            else:
                raise ValueError(f"Encountered unknown transition type "
                                 f"<{type(t)}>")

    def get_instance(self, size: int) -> instance.Instance:
        if size not in self.instance_store:
            self.instance_store[size] = instance.Instance(self.sys, size)
        return self.instance_store[size]

    async def instance_prover(self, instance_queue: asyncio.Queue) -> None:
        def _is_trap_invariant(lang: typing.List[words.Repetition]) -> bool:
            traps = all_finite_language_traps(words.Language(tuple(lang)))
            return all(is_trap(t, self.get_instance(length-2))
                       for length, t in traps)

        def _expand(lang: words.Language) -> typing.Set[words.Language]:
            res: typing.Set[words.Language] = set()
            expanding_indices = tuple(
                    i for i, rep in enumerate(lang.language)
                    if rep.minimum != rep.maximum
                    or rep.minimum != 1)
            for expansion_order in permutations(expanding_indices):
                current_lang = list(lang.language)
                for current_index in expansion_order:
                    rep = lang.language[current_index]
                    new_minimum = rep.minimum
                    while True:
                        if new_minimum == 0:
                            break
                        elif _is_trap_invariant(
                                current_lang[:current_index]
                                + [words.Repetition(
                                    rep.letter,
                                    new_minimum-1,
                                    rep.maximum)]
                                + current_lang[current_index+1:]):
                            new_minimum -= 1
                        else:
                            break
                    new_maximum = rep.maximum
                    while True:
                        if new_maximum == self.config.generalization_size:
                            break
                        elif _is_trap_invariant(
                                current_lang[:current_index]
                                + [words.Repetition(
                                    rep.letter,
                                    new_minimum,
                                    new_maximum+1)]
                                + current_lang[current_index+1:]):
                            new_maximum += 1
                        else:
                            break
                    current_lang[current_index] = words.Repetition(
                            rep.letter,
                            new_minimum,
                            new_maximum)
                res.add(words.Language(tuple(current_lang)))
            return res
        while True:
            obligation = await instance_queue.get()
            if obligation.discharged:
                continue
            size = len(obligation.instances) + 1
            instance = self.get_instance(size)
            if isinstance(obligation, LocalTransitionCheck):
                transitions = instance.get_local_transition_instances(
                        obligation.transition)
            elif isinstance(obligation, IteratingTransitionBegin):
                transitions = instance.get_iterating_transition_instances(
                        obligation.transition)[0]
            elif isinstance(obligation, IteratingTransitionFirst):
                transitions = instance.get_iterating_transition_instances(
                        obligation.transition)[1]
            elif isinstance(obligation, IteratingTransitionExit):
                transitions = instance.get_iterating_transition_instances(
                        obligation.transition)[2][obligation.exit]
            elif isinstance(obligation, IteratingTransitionProgress):
                transitions = instance.get_iterating_transition_instances(
                        obligation.transition)[3][obligation.progress_guard]
            initial_traps: typing.Set[pn.PlaceSet] = set()
            for lang in obligation.languages:
                initial_traps |= language_traps(
                        lang,
                        self.config.generalization_size,
                        size+2)
            result = await unreach(instance.initial_marking,
                                   instance.bad_markings(self.prop.name),
                                   transitions,
                                   initial_traps,
                                   instance.flows)
            if result is None:
                instance_logger.warn("Unable to establish inductiveness for "
                                     + obligation.transition.pretty
                                     + f" in size {size}")
                raise UNSATError()
            obligation.instances.append(frozenset(result | initial_traps))
            instance_logger.info("Established inductiveness for "
                                 + obligation.transition.pretty
                                 + f" in size {size}")
            languages = {
                    (words.Language.from_word(
                        words.Word.from_place_set(t, size)))
                    for t in result}
            generalizable_languages = {
                    lang.cap_repetitions(self.config.generalization_size)
                    for lang in languages
                    if any(self.config.generalization_size <= rep.maximum
                           for rep in lang.language)}
            for lang in generalizable_languages:
                # compute most general forms of this language
                obligation.add_languages(_expand(lang))
            await instance_queue.put(obligation)

    async def parameterized_prover(self, parameterized_queue) -> None:
        while not all(obligation.discharged
                      for obligation in self.obligations):
            if not parameterized_queue.empty():
                obligation = await parameterized_queue.get()
                print(f"checking transition #{obligation.transition.index} "
                      f"(reg)")
                dequeued = True
            else:
                try:
                    obligation = [obl for obl in self.obligations
                                  if not obl.discharged][-1]
                    print(f"checking transition "
                          f"#{obligation.transition.index} (ool)")
                    dequeued = False
                except IndexError:
                    continue
            if obligation.discharged:
                continue
            obligation.attempts += 1
            check = tptp_check(self.config, obligation)
            # we grow proving times to -- hopefully -- get a result eventually
            # but only if we have taken the transition out of line
            if dequeued:
                timeout = self.config.prover_timeout
            else:
                timeout = obligation.attempts*self.config.prover_timeout
            result  = await prove(check, timeout)
            if result:
                obligation.discharged = True
                parameterized_logger.info(
                        f"discharged {obligation.transition.pretty}: "  # type: ignore
                        f"type({obligation})")
                print("Transition #"
                      + str(obligation.transition.index)
                      + " proven: "
                      + str(len([o for o in self.obligations
                                 if o.discharged]))
                      + "/"
                      + str(len(self.obligations)))
            else:
                parameterized_logger.info(
                        f"re-queueing {obligation.transition.pretty}: "  # type: ignore
                        f"{obligation}")
                if dequeued:
                    await parameterized_queue.put(obligation)
        parameterized_logger.info("all obligations are discharged; setting "
                                  "the result now and stop")
        self.result = True


async def main(
        config: Config,
        sys: system.System,
        ) -> Check:
    try:
        prop_in_sys = {p.name: p for p in sys.properties}[config.prop]
    except KeyError:
        raise ValueError(f"Cannot find a property with name {config.prop}")
    check = Check(config, sys, prop_in_sys)

    # create work queues
    instance_queue: asyncio.Queue = asyncio.Queue()
    parameterized_queue: asyncio.Queue = asyncio.Queue()
    # create tasks in work queues
    for i, obligation in enumerate(check.obligations):
        instance_queue.put_nowait(obligation)
        parameterized_queue.put_nowait(obligation)

    # create proving processes
    provers = ([asyncio.create_task(check.instance_prover(instance_queue))
                for _ in range(config.instance_provers)]
               + [
                   asyncio.create_task(
                       check.parameterized_prover(parameterized_queue))
                  for _ in range(config.fo_provers)])

    # run
    try:
        done, pending = await asyncio.wait(provers,
                                           return_when=asyncio.FIRST_COMPLETED)
    except asyncio.CancelledError:
        pass
    # gracefully shutdown remaining tasks
    for future in asyncio.all_tasks():
        if future is asyncio.current_task():
            continue
        future.cancel()
    return check

import unittest
import system
import words
import templating
import algorithm


class TestWords(unittest.TestCase):
    def setUp(self):
        self.b_true = system.Value("true", 1)
        self.b_false = system.Value("false", 0)
        self.b_var = system.LocalVariable("b", (self.b_false, self.b_true))

        self.s_initial = system.Value("initial", 0)
        self.s_loop = system.Value("loop", 1)
        self.s_break = system.Value("break", 2)
        self.s_crit = system.Value("crit", 3)
        self.s_done = system.Value("done", 4)
        self.state_var = system.LocalVariable(
                "state",
                (self.s_initial,
                 self.s_loop,
                 self.s_break,
                 self.s_crit,
                 self.s_done))
        self.initial_loop = system.LocalTransition(
                "initial",
                0,
                "loop",
                tuple(),
                ((self.b_var, self.b_true),),
                tuple())
        self.iteration = system.IteratingTransition(
                "loop",
                1,
                None,
                system.IterationDirection.UP,
                (
                    system.IterationExit(
                        (
                            system.Unequal(system.other_ptr, system.self_ptr),
                            system.Equal(system.LocalTerm(self.b_var,
                                                          system.other_ptr),
                                         self.b_true)
                        ),
                        "break",
                        tuple(),
                        tuple()),
                    system.IterationExit(
                        tuple(),
                        "crit",
                        tuple(),
                        tuple())))
        self.break_initial = system.LocalTransition(
                "break",
                2,
                "initial",
                tuple(),
                ((self.b_var, self.b_false),),
                tuple())

        self.crit_done = system.LocalTransition(
                "crit",
                3,
                "done",
                tuple(),
                tuple(),
                tuple())

        self.done_initial = system.LocalTransition(
                "done",
                4,
                "initial",
                tuple(),
                ((self.b_var, self.b_false),),
                tuple())

        self.mutex = system.UnsafeProperty(
                "mutex",
                (((self.state_var, self.s_crit),),
                 ((self.state_var, self.s_crit),)),
                tuple())


        self.store = system.VariableStore(
                tuple(),
                (self.b_var, self.state_var),
                self.state_var)

        self.sys = system.System(
                self.store,
                (self.initial_loop,
                 self.iteration,
                 self.break_initial,
                 self.crit_done,
                 self.done_initial),
                (self.mutex,))

    def test_example(self):
        first = words.Language(
                (
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                words.IterationSlot(
                                    "P1",
                                    self.iteration)
                                }),
                            None),
                        1,
                        1),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                words.IterationSlot(
                                    "P1",
                                    self.iteration),
                                words.LocalVariableSlot(
                                    self.b_var,
                                    self.b_false),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_break),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_loop),
                                }),
                            "P0"),
                        1,
                        1),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                }),
                            None),
                        2,
                        2),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                words.LocalVariableSlot(
                                    self.b_var,
                                    self.b_false),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_break),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_loop),
                                }),
                            "P1"),
                        1,
                        1),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset(),
                            None),
                        2,
                        2)))
        second = words.Language(
                (
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                words.IterationSlot(
                                    "P1",
                                    self.iteration)
                                }),
                            None),
                        1,
                        1),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                words.IterationSlot(
                                    "P1",
                                    self.iteration),
                                words.LocalVariableSlot(
                                    self.b_var,
                                    self.b_false),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_break),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_loop),
                                }),
                            "P0"),
                        1,
                        1),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                }),
                            None),
                        1,
                        1),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                words.LocalVariableSlot(
                                    self.b_var,
                                    self.b_false),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_break),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_loop),
                                }),
                            "P1"),
                        1,
                        1),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset(),
                            None),
                        2,
                        2)))


        obl = algorithm.Obligation(templating.TPTPFormula(""))
        obl.add_languages({first})
        print("\n".join(l.pretty for l in obl.languages))
        obl.add_languages({second})
        print("\n".join(l.pretty for l in obl.languages))

    def test_invariant(self):
        invariant = words.Language(
                (
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                words.IterationSlot(
                                    "P1",
                                    self.iteration)
                                }),
                            None),
                        1,
                        2),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                words.IterationSlot(
                                    "P1",
                                    self.iteration),
                                words.LocalVariableSlot(
                                    self.b_var,
                                    self.b_false),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_break),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_loop),
                                }),
                            "P0"),
                        1,
                        1),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                }),
                            None),
                        0,
                        2),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset({
                                words.IterationSlot(
                                    "P0",
                                    self.iteration),
                                words.LocalVariableSlot(
                                    self.b_var,
                                    self.b_false),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_break),
                                words.LocalVariableSlot(
                                    self.state_var,
                                    self.s_loop),
                                }),
                            "P1"),
                        1,
                        1),
                    words.Repetition(
                        words.AbstractLetter(
                            frozenset(),
                            None),
                        1,
                        2)))
        # print(invariant.pretty)
        templating.render_language(invariant, 2)

if __name__ == '__main__':
    unittest.main()

import unittest
import system as sys
import templating


def _parse_file(filename):
    import json
    import parser
    with open(filename) as f:
        spec = json.load(f)
    return parser.parse_system(spec)


class TestFOF(unittest.TestCase):
    def setUp(self):
        pass

    @unittest.skip("temp")
    def test_no_turn_dijkstra(self):
        system = _parse_file("examples/no-turn-dijkstra.json")
        for t in system.transitions:
            if isinstance(t, sys.LocalTransition):
                local = templating.fof_local_transition(system, t)
                print(f"===== LOCAL =====\n{local}")
            elif isinstance(t, sys.IteratingTransition):
                begin, first, exits, proceedings = templating.fof_iterating_transition(system, t)
                print(f"===== BEGIN =====\n{begin}")
                print(f"===== FIRST =====\n{first}")
                print(f"\n".join(f"===== EXITS =====\n{e}" for e in exits))
                print(f"\n".join(f"===== PROCEEDINGS =====\n{p}"
                                 for p in proceedings))

    @unittest.skip("temp")
    def test_dijkstra(self):
        system = _parse_file("examples/dijkstra.json")
        for t in system.transitions:
            if isinstance(t, sys.LocalTransition):
                local = templating.fof_local_transition(system, t)
                print(f"===== LOCAL =====\n{local}")
            elif isinstance(t, sys.IteratingTransition):
                begin, first, exits, proceedings = templating.fof_iterating_transition(system, t)
                print(f"===== BEGIN =====\n{begin}")
                print(f"===== FIRST =====\n{first}")
                print(f"\n".join(f"===== EXITS =====\n{e}" for e in exits))
                print(f"\n".join(f"===== PROCEEDINGS =====\n{p}"
                                 for p in proceedings))

    @unittest.skip("temp")
    def test_eisenberg_mcguire(self):
        system = _parse_file("examples/eisenberg-mcguire.json")
        for t in system.transitions:
            if isinstance(t, sys.LocalTransition):
                local = templating.fof_local_transition(system, t)
                print(f"===== LOCAL =====\n{local}")
            elif isinstance(t, sys.IteratingTransition):
                begin, first, exits, proceedings = templating.fof_iterating_transition(system, t)
                print(f"===== BEGIN =====\n{begin}")
                print(f"===== FIRST =====\n{first}")
                print(f"\n".join(f"===== EXITS =====\n{e}" for e in exits))
                print(f"\n".join(f"===== PROCEEDINGS =====\n{p}"
                                 for p in proceedings))

    @unittest.skip("temp")
    def test_knuth(self):
        system = _parse_file("examples/knuth.json")
        for t in system.transitions:
            if isinstance(t, sys.LocalTransition):
                local = templating.fof_local_transition(system, t)
                print(f"===== LOCAL =====\n{local}")
            elif isinstance(t, sys.IteratingTransition):
                begin, first, exits, proceedings = templating.fof_iterating_transition(system, t)
                print(f"===== BEGIN =====\n{begin}")
                print(f"===== FIRST =====\n{first}")
                print(f"\n".join(f"===== EXITS =====\n{e}" for e in exits))
                print(f"\n".join(f"===== PROCEEDINGS =====\n{p}"
                                 for p in proceedings))


if __name__ == "__main__":
    unittest.main()

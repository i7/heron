import unittest
import instance
from processnet import algorithm as pn_alg  # type: ignore
import lark  # type: ignore

def _parse_file(filename):
    import json
    import parser
    with open(filename) as f:
        spec = json.load(f)
    return parser.parse_system(spec)

class ParserTest(unittest.TestCase):
    def test_no_turn_dijkstra(self):
        system = _parse_file("examples/no-turn-dijkstra.json")
        inst = instance.Instance(system, 3)
        res, _, inv  = pn_alg.check_reach(inst.initial_marking,
                                          inst.bad_markings("mutex"))
        self.assertFalse(res)

    def test_dijkstra(self):
        system = _parse_file("examples/dijkstra.json")
        inst = instance.Instance(system, 3)
        res, _, inv  = pn_alg.check_reach(inst.initial_marking,
                                          inst.bad_markings("mutex"))
        self.assertFalse(res)

    def test_eisenberg_mcguire(self):
        system = _parse_file("examples/eisenberg-mcguire.json")
        inst = instance.Instance(system, 3)
        res, _, inv  = pn_alg.check_reach(inst.initial_marking,
                                          inst.bad_markings("mutex"))
        self.assertFalse(res)

    def test_knuth(self):
        system = _parse_file("examples/knuth.json")
        inst = instance.Instance(system, 3)
        res, _, inv  = pn_alg.check_reach(inst.initial_marking,
                                          inst.bad_markings("mutex"))
        self.assertFalse(res)

    def test_deBruijn(self):
        system = _parse_file("examples/deBruijn.json")
        inst = instance.Instance(system, 3)
        res, _, inv  = pn_alg.check_reach(inst.initial_marking,
                                          inst.bad_markings("mutex"))
        self.assertFalse(res)


if __name__ == '__main__':
    unittest.main()

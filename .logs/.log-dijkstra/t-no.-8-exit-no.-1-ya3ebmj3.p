% BASE THEORY:

% Order, zero, next:
fof(minimal_element, axiom, ![A]: (leq(zero, A))).
fof(transitivity, axiom, ![A, B, C]: ((leq(A, B) & leq(B, C)) => leq(A, C))).
fof(reflexivity, axiom, ![A]: leq(A, A)).
fof(antisymetric, axiom, ![A, B]: (~leq(A, B) | ~leq(B, A) | B = A)).
fof(total, axiom, ![A, B]: (leq(A, B) | leq(B, A))).
fof(increasing_1, axiom, ![A]: leq(A, next(A))).
fof(increasing_1, axiom, ![A]: A != next(A)).
fof(exhausting, axiom, ![A, B]: ((leq(A, B) & A != B) => leq(next(A), B))).
fof(next_prev_identity, axiom, ![A]: (prev(next(A)) = A)).
fof(prev_next_identity, axiom, ![A]: (A = zero | (next(prev(A)) = A))).
% I don't believe we need the following axiom.
% fof(prev_zero, axiom, prev(zero) = zero).

% Introduce notion of agent:
fof(is_agent, axiom, ![A]: (is_agent(A) <=> (leq(next(zero), A) & leq(A, n)))).

% SYSTEM:

% Store

fof(ptr_value_range, axiom, is_agent(turn_before)).
fof(ptr_value_range, axiom, is_agent(turn_after)).


% iteration
fof(iteration_ptr_before_bot, axiom, ![A]: leq(zero, iteration_ptr_before(A))).
fof(iteration_ptr_before_bot, axiom, ![A]: leq(iteration_ptr_before(A), next(n))).
fof(iteration_ptr_after_bot, axiom, ![A]: leq(zero, iteration_ptr_after(A))).
fof(iteration_ptr_after_bot, axiom, ![A]: leq(iteration_ptr_after(A), next(n))).


fof(state_var_val_0, axiom, state_s1 = zero).

fof(state_var_val_1, axiom, state_s2 = next(zero)).

fof(state_var_val_2, axiom, state_s3 = next(next(zero))).

fof(state_var_val_3, axiom, state_s4 = next(next(next(zero)))).

fof(state_var_val_4, axiom, state_s5 = next(next(next(next(zero))))).

fof(state_var_val_5, axiom, state_s6 = next(next(next(next(next(zero)))))).

fof(state_var_val_6, axiom, state_s7 = next(next(next(next(next(next(zero))))))).

fof(state_var_val_7, axiom, state_s8 = next(next(next(next(next(next(next(zero)))))))).

fof(state_var_val_8, axiom, state_s8_iterating_8 = next(next(next(next(next(next(next(next(zero))))))))).

fof(state_var_val_9, axiom, state_s9 = next(next(next(next(next(next(next(next(next(zero)))))))))).

fof(state_var_val_10, axiom, state_s10 = next(next(next(next(next(next(next(next(next(next(zero))))))))))).

fof(state_var_val_11, axiom, state_s11 = next(next(next(next(next(next(next(next(next(next(next(zero)))))))))))).

fof(state_var_val_12, axiom, state_s12 = next(next(next(next(next(next(next(next(next(next(next(next(zero))))))))))))).

fof(state_var_val_13, axiom, state_s13 = next(next(next(next(next(next(next(next(next(next(next(next(next(zero)))))))))))))).


fof(state_var_values_before, axiom, ![A]: (
  $false
  
  | (state_before(A) = state_s1)
  
  | (state_before(A) = state_s2)
  
  | (state_before(A) = state_s3)
  
  | (state_before(A) = state_s4)
  
  | (state_before(A) = state_s5)
  
  | (state_before(A) = state_s6)
  
  | (state_before(A) = state_s7)
  
  | (state_before(A) = state_s8)
  
  | (state_before(A) = state_s8_iterating_8)
  
  | (state_before(A) = state_s9)
  
  | (state_before(A) = state_s10)
  
  | (state_before(A) = state_s11)
  
  | (state_before(A) = state_s12)
  
  | (state_before(A) = state_s13)
  
  )).

fof(state_var_values_after, axiom, ![A]: (
  $false
  
  | (state_after(A) = state_s1)
  
  | (state_after(A) = state_s2)
  
  | (state_after(A) = state_s3)
  
  | (state_after(A) = state_s4)
  
  | (state_after(A) = state_s5)
  
  | (state_after(A) = state_s6)
  
  | (state_after(A) = state_s7)
  
  | (state_after(A) = state_s8)
  
  | (state_after(A) = state_s8_iterating_8)
  
  | (state_after(A) = state_s9)
  
  | (state_after(A) = state_s10)
  
  | (state_after(A) = state_s11)
  
  | (state_after(A) = state_s12)
  
  | (state_after(A) = state_s13)
  
  )).




fof(local_var_val, axiom, b_true = zero).

fof(local_var_val, axiom, b_false = next(zero)).


fof(local_var_values_before, axiom, ![A]: (
  $false
  
  | b_before(A) = b_true
  
  | b_before(A) = b_false
  
  )).

fof(local_var_values_after, axiom, ![A]: (
  $false
  
  | b_after(A) = b_true
  
  | b_after(A) = b_false
  
  )).




fof(local_var_val, axiom, c_true = zero).

fof(local_var_val, axiom, c_false = next(zero)).


fof(local_var_values_before, axiom, ![A]: (
  $false
  
  | c_before(A) = c_true
  
  | c_before(A) = c_false
  
  )).

fof(local_var_values_after, axiom, ![A]: (
  $false
  
  | c_after(A) = c_true
  
  | c_after(A) = c_false
  
  )).



% theory pointer
fof(self_ptr, axiom, is_agent(self)).

% transition change
fof(current_state, axiom, state_before(self) = state_s8_iterating_8).
fof(next_state, axiom, state_after(self) = state_s11).

% render guard

fof(comp_exceeded, axiom, iteration_ptr_before(self) = next(n)).


% ensure no change for everything but other and self

fof(unchanged_b, axiom, ![A]: (A = self | A = iteration_ptr_before(self) | b_after(A) = b_before(A))).

fof(unchanged_c, axiom, ![A]: (A = self | A = iteration_ptr_before(self) | c_after(A) = c_before(A))).

fof(unchanged_state, axiom, ![A]: (A = self | A = iteration_ptr_before(self) | state_after(A) = state_before(A))).


% self does not change except for state

fof(unchanged_b, axiom, b_after(self) = b_before(self)).

fof(unchanged_c, axiom, c_after(self) = c_before(self)).


% iteration_ptr should not change either since we exit here
fof(unchanged_iteration_ptr, axiom, ![A]: (iteration_ptr_after(A) = iteration_ptr_before(A))).


% ensure no change for anything that is not assigned

fof(unchanged_b_other, axiom, b_after(iteration_ptr_before(self)) = b_before(iteration_ptr_before(self))).

fof(unchanged_c_other, axiom, c_after(iteration_ptr_before(self)) = c_before(iteration_ptr_before(self))).

fof(unchanged_state_other, axiom, state_after(iteration_ptr_before(self)) = state_before(iteration_ptr_before(self))).


fof(unchanged_turn, axiom, turn_after = turn_before).


% assignments



fof(mutex_before, axiom, ~(
  ?[P1, P2]: (
    $true
    
    & (
      is_agent(P1)
      
      
      
      & state_before(P1) = state_s11
      
    )
    
    & (
      is_agent(P2)
      
      & leq(P1, P2) & P1 != P2
      
      
      
      & state_before(P2) = state_s11
      
    )
    
    
  )
)).

fof(mutex_after, conjecture, ~(
  ?[P1, P2]: (
    $true
    
    & (
      is_agent(P1)
      
      
      
      & state_after(P1) = state_s11
      
    )
    
    & (
      is_agent(P2)
      
      & leq(P1, P2) & P1 != P2
      
      
      
      & state_after(P2) = state_s11
      
    )
    
    
  )
)).

fof(min_size, axiom, leq(next(next(next(next(next(next(next(next(next(zero))))))))), n)).
% {}[1, 3]{b = true, c = false, state = s12, state = s2, state = s3, state = s4, state = s5, state = s6, state = s7}[1, 1]{}[1, 3]
fof(inv_0_before, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_before(Q0) = state_s12) | (state_before(Q0) = state_s6) | (b_before(Q0) = b_true) | (state_before(Q0) = state_s5) | (c_before(Q0) = c_false) | (state_before(Q0) = state_s4) | (state_before(Q0) = state_s7) | (state_before(Q0) = state_s2) | (state_before(Q0) = state_s3)))).
fof(inv_0_after, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_after(Q0) = state_s12) | (state_after(Q0) = state_s6) | (b_after(Q0) = b_true) | (state_after(Q0) = state_s5) | (c_after(Q0) = c_false) | (state_after(Q0) = state_s4) | (state_after(Q0) = state_s7) | (state_after(Q0) = state_s2) | (state_after(Q0) = state_s3)))).
% {t_8[P0], t_8[P1]}[1, 3]P0:{c = true, state = s10, state = s8, t_8[P0], t_8[P1]}[1, 1]{t_8[P0]}[0, 3]P1:{c = true, state = s10, state = s8, t_8[P0]}[1, 1]{}[1, 3]
fof(inv_1_before, axiom, ![Q0, Q1, Q2]: ((Q2 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1) & leq(next(Q1), Q2)) => ((state_before(Q0) = state_s8_iterating_8 & ((leq(zero, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), Q1)))) | (state_before(Q1) = state_s8_iterating_8 & ((leq(zero, iteration_ptr_before(Q1)) & leq(iteration_ptr_before(Q1), Q0)))) | (c_before(Q0) = c_true) | (state_before(Q0) = state_s10) | (state_before(Q0) = state_s8) | (c_before(Q1) = c_true) | (state_before(Q1) = state_s10) | (state_before(Q1) = state_s8)))).
fof(inv_1_after, axiom, ![Q0, Q1, Q2]: ((Q2 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1) & leq(next(Q1), Q2)) => ((state_after(Q0) = state_s8_iterating_8 & ((leq(zero, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), Q1)))) | (state_after(Q1) = state_s8_iterating_8 & ((leq(zero, iteration_ptr_after(Q1)) & leq(iteration_ptr_after(Q1), Q0)))) | (c_after(Q0) = c_true) | (state_after(Q0) = state_s10) | (state_after(Q0) = state_s8) | (c_after(Q1) = c_true) | (state_after(Q1) = state_s10) | (state_after(Q1) = state_s8)))).
% {t_8[P0]}[1, 1]{t_8[P0], turn}[1, 1]P0:{b = true, state = s10, state = s11, state = s12, state = s2, state = s6, state = s7, state = s8, t_8[P0]}[1, 1]{t_8[P0], turn}[0, 3]{t_8[P0]}[1, 1]
fof(inv_2_before, axiom, ![Q0]: ((Q0 = next(n) & leq(next(next(next(zero))), Q0)) => ((state_before(next(next(zero))) = state_s8_iterating_8 & ((leq(zero, iteration_ptr_before(next(next(zero)))) & leq(iteration_ptr_before(next(next(zero))), Q0)))) | (((leq(next(zero), turn_before) & leq(turn_before, next(zero))) | (leq(next(next(next(zero))), turn_before) & leq(turn_before, Q0)))) | (state_before(next(next(zero))) = state_s6) | (state_before(next(next(zero))) = state_s8) | (b_before(next(next(zero))) = b_true) | (state_before(next(next(zero))) = state_s11) | (state_before(next(next(zero))) = state_s10) | (state_before(next(next(zero))) = state_s12) | (state_before(next(next(zero))) = state_s7) | (state_before(next(next(zero))) = state_s2)))).
fof(inv_2_after, axiom, ![Q0]: ((Q0 = next(n) & leq(next(next(next(zero))), Q0)) => ((state_after(next(next(zero))) = state_s8_iterating_8 & ((leq(zero, iteration_ptr_after(next(next(zero)))) & leq(iteration_ptr_after(next(next(zero))), Q0)))) | (((leq(next(zero), turn_after) & leq(turn_after, next(zero))) | (leq(next(next(next(zero))), turn_after) & leq(turn_after, Q0)))) | (state_after(next(next(zero))) = state_s6) | (state_after(next(next(zero))) = state_s8) | (b_after(next(next(zero))) = b_true) | (state_after(next(next(zero))) = state_s11) | (state_after(next(next(zero))) = state_s10) | (state_after(next(next(zero))) = state_s12) | (state_after(next(next(zero))) = state_s7) | (state_after(next(next(zero))) = state_s2)))).
% {t_8[P0]}[1, 3]P0:{c = true, state = s10, state = s11, state = s8, t_8[P0]}[1, 1]{t_8[P0]}[1, 3]
fof(inv_3_before, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_before(Q0) = state_s8_iterating_8 & ((leq(zero, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), next(n))))) | (c_before(Q0) = c_true) | (state_before(Q0) = state_s10) | (state_before(Q0) = state_s8) | (state_before(Q0) = state_s11)))).
fof(inv_3_after, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_after(Q0) = state_s8_iterating_8 & ((leq(zero, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), next(n))))) | (c_after(Q0) = c_true) | (state_after(Q0) = state_s10) | (state_after(Q0) = state_s8) | (state_after(Q0) = state_s11)))).
% {}[1, 3]{b = false, state = s1, state = s13}[1, 1]{}[1, 3]
fof(inv_4_before, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_before(Q0) = state_s1) | (b_before(Q0) = b_false) | (state_before(Q0) = state_s13)))).
fof(inv_4_after, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_after(Q0) = state_s1) | (b_after(Q0) = b_false) | (state_after(Q0) = state_s13)))).

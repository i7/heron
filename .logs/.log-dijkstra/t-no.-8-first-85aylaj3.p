% BASE THEORY:

% Order, zero, next:
fof(minimal_element, axiom, ![A]: (leq(zero, A))).
fof(transitivity, axiom, ![A, B, C]: ((leq(A, B) & leq(B, C)) => leq(A, C))).
fof(reflexivity, axiom, ![A]: leq(A, A)).
fof(antisymetric, axiom, ![A, B]: (~leq(A, B) | ~leq(B, A) | B = A)).
fof(total, axiom, ![A, B]: (leq(A, B) | leq(B, A))).
fof(increasing_1, axiom, ![A]: leq(A, next(A))).
fof(increasing_1, axiom, ![A]: A != next(A)).
fof(exhausting, axiom, ![A, B]: ((leq(A, B) & A != B) => leq(next(A), B))).
fof(next_prev_identity, axiom, ![A]: (prev(next(A)) = A)).
fof(prev_next_identity, axiom, ![A]: (A = zero | (next(prev(A)) = A))).
% I don't believe we need the following axiom.
% fof(prev_zero, axiom, prev(zero) = zero).

% Introduce notion of agent:
fof(is_agent, axiom, ![A]: (is_agent(A) <=> (leq(next(zero), A) & leq(A, n)))).

% SYSTEM:

% Store

fof(ptr_value_range, axiom, is_agent(turn_before)).
fof(ptr_value_range, axiom, is_agent(turn_after)).


% iteration
fof(iteration_ptr_before_bot, axiom, ![A]: leq(zero, iteration_ptr_before(A))).
fof(iteration_ptr_before_bot, axiom, ![A]: leq(iteration_ptr_before(A), next(n))).
fof(iteration_ptr_after_bot, axiom, ![A]: leq(zero, iteration_ptr_after(A))).
fof(iteration_ptr_after_bot, axiom, ![A]: leq(iteration_ptr_after(A), next(n))).


fof(state_var_val_0, axiom, state_s1 = zero).

fof(state_var_val_1, axiom, state_s2 = next(zero)).

fof(state_var_val_2, axiom, state_s3 = next(next(zero))).

fof(state_var_val_3, axiom, state_s4 = next(next(next(zero)))).

fof(state_var_val_4, axiom, state_s5 = next(next(next(next(zero))))).

fof(state_var_val_5, axiom, state_s6 = next(next(next(next(next(zero)))))).

fof(state_var_val_6, axiom, state_s7 = next(next(next(next(next(next(zero))))))).

fof(state_var_val_7, axiom, state_s8 = next(next(next(next(next(next(next(zero)))))))).

fof(state_var_val_8, axiom, state_s8_iterating_8 = next(next(next(next(next(next(next(next(zero))))))))).

fof(state_var_val_9, axiom, state_s9 = next(next(next(next(next(next(next(next(next(zero)))))))))).

fof(state_var_val_10, axiom, state_s10 = next(next(next(next(next(next(next(next(next(next(zero))))))))))).

fof(state_var_val_11, axiom, state_s11 = next(next(next(next(next(next(next(next(next(next(next(zero)))))))))))).

fof(state_var_val_12, axiom, state_s12 = next(next(next(next(next(next(next(next(next(next(next(next(zero))))))))))))).

fof(state_var_val_13, axiom, state_s13 = next(next(next(next(next(next(next(next(next(next(next(next(next(zero)))))))))))))).


fof(state_var_values_before, axiom, ![A]: (
  $false
  
  | (state_before(A) = state_s1)
  
  | (state_before(A) = state_s2)
  
  | (state_before(A) = state_s3)
  
  | (state_before(A) = state_s4)
  
  | (state_before(A) = state_s5)
  
  | (state_before(A) = state_s6)
  
  | (state_before(A) = state_s7)
  
  | (state_before(A) = state_s8)
  
  | (state_before(A) = state_s8_iterating_8)
  
  | (state_before(A) = state_s9)
  
  | (state_before(A) = state_s10)
  
  | (state_before(A) = state_s11)
  
  | (state_before(A) = state_s12)
  
  | (state_before(A) = state_s13)
  
  )).

fof(state_var_values_after, axiom, ![A]: (
  $false
  
  | (state_after(A) = state_s1)
  
  | (state_after(A) = state_s2)
  
  | (state_after(A) = state_s3)
  
  | (state_after(A) = state_s4)
  
  | (state_after(A) = state_s5)
  
  | (state_after(A) = state_s6)
  
  | (state_after(A) = state_s7)
  
  | (state_after(A) = state_s8)
  
  | (state_after(A) = state_s8_iterating_8)
  
  | (state_after(A) = state_s9)
  
  | (state_after(A) = state_s10)
  
  | (state_after(A) = state_s11)
  
  | (state_after(A) = state_s12)
  
  | (state_after(A) = state_s13)
  
  )).




fof(local_var_val, axiom, b_true = zero).

fof(local_var_val, axiom, b_false = next(zero)).


fof(local_var_values_before, axiom, ![A]: (
  $false
  
  | b_before(A) = b_true
  
  | b_before(A) = b_false
  
  )).

fof(local_var_values_after, axiom, ![A]: (
  $false
  
  | b_after(A) = b_true
  
  | b_after(A) = b_false
  
  )).




fof(local_var_val, axiom, c_true = zero).

fof(local_var_val, axiom, c_false = next(zero)).


fof(local_var_values_before, axiom, ![A]: (
  $false
  
  | c_before(A) = c_true
  
  | c_before(A) = c_false
  
  )).

fof(local_var_values_after, axiom, ![A]: (
  $false
  
  | c_after(A) = c_true
  
  | c_after(A) = c_false
  
  )).



% theory pointer
fof(self_ptr, axiom, is_agent(self)).

fof(current_state, axiom, state_before(self) = state_s8_iterating_8).
fof(current_state, axiom, state_after(self) = state_s8_iterating_8).


fof(check_iteration_ptr, axiom, iteration_ptr_before(self) = zero).
fof(set_iteration_ptr, axiom, iteration_ptr_after(self) = next(zero)).


% ensure no change for everything

fof(unchanged_b, axiom, ![A]: (b_after(A) = b_before(A))).

fof(unchanged_c, axiom, ![A]: (c_after(A) = c_before(A))).


% ensure no state change for everything but self
fof(unchanged_state, axiom, ![A]: (A = self | state_after(A) = state_before(A))).


fof(unchanged_turn, axiom, turn_after = turn_before).


fof(mutex_before, axiom, ~(
  ?[P1, P2]: (
    $true
    
    & (
      is_agent(P1)
      
      
      
      & state_before(P1) = state_s11
      
    )
    
    & (
      is_agent(P2)
      
      & leq(P1, P2) & P1 != P2
      
      
      
      & state_before(P2) = state_s11
      
    )
    
    
  )
)).

fof(mutex_after, conjecture, ~(
  ?[P1, P2]: (
    $true
    
    & (
      is_agent(P1)
      
      
      
      & state_after(P1) = state_s11
      
    )
    
    & (
      is_agent(P2)
      
      & leq(P1, P2) & P1 != P2
      
      
      
      & state_after(P2) = state_s11
      
    )
    
    
  )
)).

fof(min_size, axiom, leq(next(next(next(next(zero)))), n)).

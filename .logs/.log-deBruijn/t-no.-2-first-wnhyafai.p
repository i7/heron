% BASE THEORY:

% Order, zero, next:
fof(minimal_element, axiom, ![A]: (leq(zero, A))).
fof(transitivity, axiom, ![A, B, C]: ((leq(A, B) & leq(B, C)) => leq(A, C))).
fof(reflexivity, axiom, ![A]: leq(A, A)).
fof(antisymetric, axiom, ![A, B]: (~leq(A, B) | ~leq(B, A) | B = A)).
fof(total, axiom, ![A, B]: (leq(A, B) | leq(B, A))).
fof(increasing_1, axiom, ![A]: leq(A, next(A))).
fof(increasing_1, axiom, ![A]: A != next(A)).
fof(exhausting, axiom, ![A, B]: ((leq(A, B) & A != B) => leq(next(A), B))).
fof(next_prev_identity, axiom, ![A]: (prev(next(A)) = A)).
fof(prev_next_identity, axiom, ![A]: (A = zero | (next(prev(A)) = A))).
% I don't believe we need the following axiom.
% fof(prev_zero, axiom, prev(zero) = zero).

% Introduce notion of agent:
fof(is_agent, axiom, ![A]: (is_agent(A) <=> (leq(next(zero), A) & leq(A, n)))).

% SYSTEM:

% Store

fof(ptr_value_range, axiom, is_agent(k_before)).
fof(ptr_value_range, axiom, is_agent(k_after)).


% iteration
fof(iteration_ptr_before_bot, axiom, ![A]: leq(zero, iteration_ptr_before(A))).
fof(iteration_ptr_before_bot, axiom, ![A]: leq(iteration_ptr_before(A), next(n))).
fof(iteration_ptr_after_bot, axiom, ![A]: leq(zero, iteration_ptr_after(A))).
fof(iteration_ptr_after_bot, axiom, ![A]: leq(iteration_ptr_after(A), next(n))).


fof(state_var_val_0, axiom, state_initial = zero).

fof(state_var_val_1, axiom, state_passFirstCheck = next(zero)).

fof(state_var_val_2, axiom, state_CS = next(next(zero))).

fof(state_var_val_3, axiom, state_loop1_init = next(next(next(zero)))).

fof(state_var_val_4, axiom, state_loop1_init_iterating_1 = next(next(next(next(zero))))).

fof(state_var_val_5, axiom, state_loop2_init = next(next(next(next(next(zero)))))).

fof(state_var_val_6, axiom, state_loop2_init_iterating_2 = next(next(next(next(next(next(zero))))))).

fof(state_var_val_7, axiom, state_loop3_init = next(next(next(next(next(next(next(zero)))))))).

fof(state_var_val_8, axiom, state_loop3_init_iterating_4 = next(next(next(next(next(next(next(next(zero))))))))).


fof(state_var_values_before, axiom, ![A]: (
  $false
  
  | (state_before(A) = state_initial)
  
  | (state_before(A) = state_passFirstCheck)
  
  | (state_before(A) = state_CS)
  
  | (state_before(A) = state_loop1_init)
  
  | (state_before(A) = state_loop1_init_iterating_1)
  
  | (state_before(A) = state_loop2_init)
  
  | (state_before(A) = state_loop2_init_iterating_2)
  
  | (state_before(A) = state_loop3_init)
  
  | (state_before(A) = state_loop3_init_iterating_4)
  
  )).

fof(state_var_values_after, axiom, ![A]: (
  $false
  
  | (state_after(A) = state_initial)
  
  | (state_after(A) = state_passFirstCheck)
  
  | (state_after(A) = state_CS)
  
  | (state_after(A) = state_loop1_init)
  
  | (state_after(A) = state_loop1_init_iterating_1)
  
  | (state_after(A) = state_loop2_init)
  
  | (state_after(A) = state_loop2_init_iterating_2)
  
  | (state_after(A) = state_loop3_init)
  
  | (state_after(A) = state_loop3_init_iterating_4)
  
  )).




fof(local_var_val, axiom, control_neutral = zero).

fof(local_var_val, axiom, control_firstLevel = next(zero)).

fof(local_var_val, axiom, control_secondLevel = next(next(zero))).


fof(local_var_values_before, axiom, ![A]: (
  $false
  
  | control_before(A) = control_neutral
  
  | control_before(A) = control_firstLevel
  
  | control_before(A) = control_secondLevel
  
  )).

fof(local_var_values_after, axiom, ![A]: (
  $false
  
  | control_after(A) = control_neutral
  
  | control_after(A) = control_firstLevel
  
  | control_after(A) = control_secondLevel
  
  )).



% theory pointer
fof(self_ptr, axiom, is_agent(self)).

fof(current_state, axiom, state_before(self) = state_loop2_init_iterating_2).
fof(current_state, axiom, state_after(self) = state_loop2_init_iterating_2).


fof(check_iteration_ptr, axiom, iteration_ptr_before(self) = next(n)).
fof(set_iteration_ptr, axiom, iteration_ptr_after(self) = n).


% ensure no change for everything

fof(unchanged_control, axiom, ![A]: (control_after(A) = control_before(A))).


% ensure no state change for everything but self
fof(unchanged_state, axiom, ![A]: (A = self | state_after(A) = state_before(A))).


fof(unchanged_k, axiom, k_after = k_before).


fof(mutex_before, axiom, ~(
  ?[P1, P2]: (
    $true
    
    & (
      is_agent(P1)
      
      
      
      & state_before(P1) = state_CS
      
    )
    
    & (
      is_agent(P2)
      
      & leq(P1, P2) & P1 != P2
      
      
      
      & state_before(P2) = state_CS
      
    )
    
    
  )
)).

fof(mutex_after, conjecture, ~(
  ?[P1, P2]: (
    $true
    
    & (
      is_agent(P1)
      
      
      
      & state_after(P1) = state_CS
      
    )
    
    & (
      is_agent(P2)
      
      & leq(P1, P2) & P1 != P2
      
      
      
      & state_after(P2) = state_CS
      
    )
    
    
  )
)).

fof(min_size, axiom, leq(next(next(next(next(zero)))), n)).

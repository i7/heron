% BASE THEORY:

% Order, zero, next:
fof(minimal_element, axiom, ![A]: (leq(zero, A))).
fof(transitivity, axiom, ![A, B, C]: ((leq(A, B) & leq(B, C)) => leq(A, C))).
fof(reflexivity, axiom, ![A]: leq(A, A)).
fof(antisymetric, axiom, ![A, B]: (~leq(A, B) | ~leq(B, A) | B = A)).
fof(total, axiom, ![A, B]: (leq(A, B) | leq(B, A))).
fof(increasing_1, axiom, ![A]: leq(A, next(A))).
fof(increasing_1, axiom, ![A]: A != next(A)).
fof(exhausting, axiom, ![A, B]: ((leq(A, B) & A != B) => leq(next(A), B))).
fof(next_prev_identity, axiom, ![A]: (prev(next(A)) = A)).
fof(prev_next_identity, axiom, ![A]: (A = zero | (next(prev(A)) = A))).
% I don't believe we need the following axiom.
% fof(prev_zero, axiom, prev(zero) = zero).

% Introduce notion of agent:
fof(is_agent, axiom, ![A]: (is_agent(A) <=> (leq(next(zero), A) & leq(A, n)))).

% SYSTEM:

% Store

fof(ptr_value_range, axiom, is_agent(k_before)).
fof(ptr_value_range, axiom, is_agent(k_after)).


% iteration
fof(iteration_ptr_before_bot, axiom, ![A]: leq(zero, iteration_ptr_before(A))).
fof(iteration_ptr_before_bot, axiom, ![A]: leq(iteration_ptr_before(A), next(n))).
fof(iteration_ptr_after_bot, axiom, ![A]: leq(zero, iteration_ptr_after(A))).
fof(iteration_ptr_after_bot, axiom, ![A]: leq(iteration_ptr_after(A), next(n))).


fof(state_var_val_0, axiom, state_initial = zero).

fof(state_var_val_1, axiom, state_loop1_init = next(zero)).

fof(state_var_val_2, axiom, state_loop1_init_iterating_1 = next(next(zero))).

fof(state_var_val_3, axiom, state_loop2_init = next(next(next(zero)))).

fof(state_var_val_4, axiom, state_loop2_init_iterating_2 = next(next(next(next(zero))))).

fof(state_var_val_5, axiom, state_loop3_init = next(next(next(next(next(zero)))))).

fof(state_var_val_6, axiom, state_loop3_init_iterating_4 = next(next(next(next(next(next(zero))))))).

fof(state_var_val_7, axiom, state_passFirstCheck = next(next(next(next(next(next(next(zero)))))))).

fof(state_var_val_8, axiom, state_setK = next(next(next(next(next(next(next(next(zero))))))))).

fof(state_var_val_9, axiom, state_CS = next(next(next(next(next(next(next(next(next(zero)))))))))).


fof(state_var_values_before, axiom, ![A]: (
  $false
  
  | (state_before(A) = state_initial)
  
  | (state_before(A) = state_loop1_init)
  
  | (state_before(A) = state_loop1_init_iterating_1)
  
  | (state_before(A) = state_loop2_init)
  
  | (state_before(A) = state_loop2_init_iterating_2)
  
  | (state_before(A) = state_loop3_init)
  
  | (state_before(A) = state_loop3_init_iterating_4)
  
  | (state_before(A) = state_passFirstCheck)
  
  | (state_before(A) = state_setK)
  
  | (state_before(A) = state_CS)
  
  )).

fof(state_var_values_after, axiom, ![A]: (
  $false
  
  | (state_after(A) = state_initial)
  
  | (state_after(A) = state_loop1_init)
  
  | (state_after(A) = state_loop1_init_iterating_1)
  
  | (state_after(A) = state_loop2_init)
  
  | (state_after(A) = state_loop2_init_iterating_2)
  
  | (state_after(A) = state_loop3_init)
  
  | (state_after(A) = state_loop3_init_iterating_4)
  
  | (state_after(A) = state_passFirstCheck)
  
  | (state_after(A) = state_setK)
  
  | (state_after(A) = state_CS)
  
  )).




fof(local_var_val, axiom, control_neutral = zero).

fof(local_var_val, axiom, control_firstLevel = next(zero)).

fof(local_var_val, axiom, control_secondLevel = next(next(zero))).


fof(local_var_values_before, axiom, ![A]: (
  $false
  
  | control_before(A) = control_neutral
  
  | control_before(A) = control_firstLevel
  
  | control_before(A) = control_secondLevel
  
  )).

fof(local_var_values_after, axiom, ![A]: (
  $false
  
  | control_after(A) = control_neutral
  
  | control_after(A) = control_firstLevel
  
  | control_after(A) = control_secondLevel
  
  )).



% theory pointer
fof(self_ptr, axiom, is_agent(self)).

% transition change
fof(current_state, axiom, state_before(self) = state_setK).
fof(next_state, axiom, state_after(self) = state_CS).

% render guard


% ensure no change for everything but self
fof(unchanged_iteration_ptr, axiom, ![A]: (iteration_ptr_after(A) = iteration_ptr_before(A))).

fof(unchanged_control, axiom, ![A]: (A = self | control_after(A) = control_before(A))).

fof(unchanged_state, axiom, ![A]: (A = self | state_after(A) = state_before(A))).



% ensure no change for anything that is not assigned

fof(unchanged_control_self, axiom, control_after(self) = control_before(self)).



% assignments


fof(change_k, axiom, k_after = self).


fof(mutex_before, axiom, ~(
  ?[P1, P2]: (
    $true
    
    & (
      is_agent(P1)
      
      
      
      & state_before(P1) = state_CS
      
    )
    
    & (
      is_agent(P2)
      
      & leq(P1, P2) & P1 != P2
      
      
      
      & state_before(P2) = state_CS
      
    )
    
    
  )
)).

fof(mutex_after, conjecture, ~(
  ?[P1, P2]: (
    $true
    
    & (
      is_agent(P1)
      
      
      
      & state_after(P1) = state_CS
      
    )
    
    & (
      is_agent(P2)
      
      & leq(P1, P2) & P1 != P2
      
      
      
      & state_after(P2) = state_CS
      
    )
    
    
  )
)).

fof(min_size, axiom, leq(next(next(next(next(next(next(next(next(next(zero))))))))), n)).
% {}[1, 4]P0:{control = firstLevel, control = neutral, state = initial, state = loop3_init, t_4[P1]}[1, 1]{t_4[P1]}[0, 4]P1:{control = firstLevel, control = neutral, state = initial, state = loop3_init, t_4[P0], t_4[P1]}[1, 1]{t_4[P0], t_4[P1]}[1, 4]
fof(inv_0_before, axiom, ![Q0, Q1, Q2]: ((Q2 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1) & leq(next(Q1), Q2)) => ((state_before(Q0) = state_loop3_init_iterating_4 & ((leq(Q1, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), next(n))))) | (state_before(Q1) = state_loop3_init_iterating_4 & ((leq(Q0, iteration_ptr_before(Q1)) & leq(iteration_ptr_before(Q1), next(n))))) | (control_before(Q0) = control_firstLevel) | (state_before(Q0) = state_loop3_init) | (state_before(Q0) = state_initial) | (control_before(Q0) = control_neutral) | (state_before(Q1) = state_initial) | (control_before(Q1) = control_firstLevel) | (state_before(Q1) = state_loop3_init) | (control_before(Q1) = control_neutral)))).
fof(inv_0_after, axiom, ![Q0, Q1, Q2]: ((Q2 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1) & leq(next(Q1), Q2)) => ((state_after(Q0) = state_loop3_init_iterating_4 & ((leq(Q1, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), next(n))))) | (state_after(Q1) = state_loop3_init_iterating_4 & ((leq(Q0, iteration_ptr_after(Q1)) & leq(iteration_ptr_after(Q1), next(n))))) | (control_after(Q0) = control_firstLevel) | (state_after(Q0) = state_loop3_init) | (state_after(Q0) = state_initial) | (control_after(Q0) = control_neutral) | (state_after(Q1) = state_initial) | (control_after(Q1) = control_firstLevel) | (state_after(Q1) = state_loop3_init) | (control_after(Q1) = control_neutral)))).
% {t_1[P0]}[1, 4]P0:{control = secondLevel, state = initial, state = loop1_init, state = loop2_init, state = passFirstCheck, t_1[P0], t_2[P0]}[1, 1]{t_1[P0], t_2[P0]}[0, 4]{t_2[P0]}[1, 1]
fof(inv_1_before, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_before(Q0) = state_loop1_init_iterating_1 & ((leq(zero, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), Q1)))) | (state_before(Q0) = state_loop2_init_iterating_2 & ((leq(Q0, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), Q1)))) | (state_before(Q0) = state_passFirstCheck) | (state_before(Q0) = state_initial) | (state_before(Q0) = state_loop1_init) | (state_before(Q0) = state_loop2_init) | (control_before(Q0) = control_secondLevel)))).
fof(inv_1_after, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_after(Q0) = state_loop1_init_iterating_1 & ((leq(zero, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), Q1)))) | (state_after(Q0) = state_loop2_init_iterating_2 & ((leq(Q0, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), Q1)))) | (state_after(Q0) = state_passFirstCheck) | (state_after(Q0) = state_initial) | (state_after(Q0) = state_loop1_init) | (state_after(Q0) = state_loop2_init) | (control_after(Q0) = control_secondLevel)))).
% {t_1[P0]}[1, 4]P0:{control = neutral, control = secondLevel, state = loop1_init, state = loop2_init, state = passFirstCheck, t_1[P0], t_2[P0]}[1, 1]{t_1[P0], t_2[P0]}[0, 4]{t_2[P0]}[1, 1]
fof(inv_2_before, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_before(Q0) = state_loop1_init_iterating_1 & ((leq(zero, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), Q1)))) | (state_before(Q0) = state_loop2_init_iterating_2 & ((leq(Q0, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), Q1)))) | (state_before(Q0) = state_passFirstCheck) | (state_before(Q0) = state_loop1_init) | (state_before(Q0) = state_loop2_init) | (control_before(Q0) = control_neutral) | (control_before(Q0) = control_secondLevel)))).
fof(inv_2_after, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_after(Q0) = state_loop1_init_iterating_1 & ((leq(zero, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), Q1)))) | (state_after(Q0) = state_loop2_init_iterating_2 & ((leq(Q0, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), Q1)))) | (state_after(Q0) = state_passFirstCheck) | (state_after(Q0) = state_loop1_init) | (state_after(Q0) = state_loop2_init) | (control_after(Q0) = control_neutral) | (control_after(Q0) = control_secondLevel)))).
% {t_4[P0]}[1, 4]P0:{control = firstLevel, state = CS, state = initial, state = loop3_init, state = setK, t_4[P0]}[1, 1]{t_4[P0]}[1, 1]
fof(inv_3_before, axiom, ![Q0]: ((next(Q0) = next(n) & leq(next(zero), Q0)) => ((state_before(Q0) = state_loop3_init_iterating_4 & ((leq(zero, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), next(Q0))))) | (state_before(Q0) = state_loop3_init) | (state_before(Q0) = state_CS) | (state_before(Q0) = state_setK) | (control_before(Q0) = control_firstLevel) | (state_before(Q0) = state_initial)))).
fof(inv_3_after, axiom, ![Q0]: ((next(Q0) = next(n) & leq(next(zero), Q0)) => ((state_after(Q0) = state_loop3_init_iterating_4 & ((leq(zero, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), next(Q0))))) | (state_after(Q0) = state_loop3_init) | (state_after(Q0) = state_CS) | (state_after(Q0) = state_setK) | (control_after(Q0) = control_firstLevel) | (state_after(Q0) = state_initial)))).
% {t_1[P0], t_4[P0]}[1, 4]P0:{state = CS, state = initial, state = loop1_init, state = loop2_init, state = loop3_init, state = passFirstCheck, state = setK, t_1[P0], t_2[P0], t_4[P0]}[1, 1]{t_1[P0], t_2[P0], t_4[P0]}[0, 4]{t_2[P0], t_4[P0]}[1, 1]
fof(inv_4_before, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_before(Q0) = state_loop3_init_iterating_4 & ((leq(zero, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), Q1)))) | (state_before(Q0) = state_loop1_init_iterating_1 & ((leq(zero, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), Q1)))) | (state_before(Q0) = state_loop2_init_iterating_2 & ((leq(Q0, iteration_ptr_before(Q0)) & leq(iteration_ptr_before(Q0), Q1)))) | (state_before(Q0) = state_CS) | (state_before(Q0) = state_setK) | (state_before(Q0) = state_initial) | (state_before(Q0) = state_loop3_init) | (state_before(Q0) = state_loop1_init) | (state_before(Q0) = state_loop2_init) | (state_before(Q0) = state_passFirstCheck)))).
fof(inv_4_after, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_after(Q0) = state_loop3_init_iterating_4 & ((leq(zero, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), Q1)))) | (state_after(Q0) = state_loop1_init_iterating_1 & ((leq(zero, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), Q1)))) | (state_after(Q0) = state_loop2_init_iterating_2 & ((leq(Q0, iteration_ptr_after(Q0)) & leq(iteration_ptr_after(Q0), Q1)))) | (state_after(Q0) = state_CS) | (state_after(Q0) = state_setK) | (state_after(Q0) = state_initial) | (state_after(Q0) = state_loop3_init) | (state_after(Q0) = state_loop1_init) | (state_after(Q0) = state_loop2_init) | (state_after(Q0) = state_passFirstCheck)))).
% {}[1, 4]{control = firstLevel, control = secondLevel, state = initial}[1, 1]{}[1, 4]
fof(inv_5_before, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_before(Q0) = state_initial) | (control_before(Q0) = control_firstLevel) | (control_before(Q0) = control_secondLevel)))).
fof(inv_5_after, axiom, ![Q0, Q1]: ((Q1 = next(n) & leq(next(zero), Q0) & leq(next(Q0), Q1)) => ((state_after(Q0) = state_initial) | (control_after(Q0) = control_firstLevel) | (control_after(Q0) = control_secondLevel)))).
% {}[1, 1]P0:{control = secondLevel, state = initial, state = loop1_init, state = passFirstCheck, t_1[P0]}[1, 1]{t_1[P0]}[0, 4]{}[1, 1]
fof(inv_6_before, axiom, ![Q0]: ((Q0 = next(n) & leq(next(next(zero)), Q0)) => ((state_before(next(zero)) = state_loop1_init_iterating_1 & ((leq(next(zero), iteration_ptr_before(next(zero))) & leq(iteration_ptr_before(next(zero)), Q0)))) | (state_before(next(zero)) = state_passFirstCheck) | (state_before(next(zero)) = state_initial) | (state_before(next(zero)) = state_loop1_init) | (control_before(next(zero)) = control_secondLevel)))).
fof(inv_6_after, axiom, ![Q0]: ((Q0 = next(n) & leq(next(next(zero)), Q0)) => ((state_after(next(zero)) = state_loop1_init_iterating_1 & ((leq(next(zero), iteration_ptr_after(next(zero))) & leq(iteration_ptr_after(next(zero)), Q0)))) | (state_after(next(zero)) = state_passFirstCheck) | (state_after(next(zero)) = state_initial) | (state_after(next(zero)) = state_loop1_init) | (control_after(next(zero)) = control_secondLevel)))).

% BASE THEORY:

% Order, zero, next:
fof(minimal_element, axiom, ![A]: (leq(zero, A))).
fof(transitivity, axiom, ![A, B, C]: ((leq(A, B) & leq(B, C)) => leq(A, C))).
fof(reflexivity, axiom, ![A]: leq(A, A)).
fof(antisymetric, axiom, ![A, B]: (~leq(A, B) | ~leq(B, A) | B = A)).
fof(total, axiom, ![A, B]: (leq(A, B) | leq(B, A))).
fof(increasing_1, axiom, ![A]: leq(A, next(A))).
fof(increasing_1, axiom, ![A]: A != next(A)).
fof(exhausting, axiom, ![A, B]: ((leq(A, B) & A != B) => leq(next(A), B))).
fof(next_prev_identity, axiom, ![A]: (prev(next(A)) = A)).
fof(prev_next_identity, axiom, ![A]: (A = zero | (next(prev(A)) = A))).
% I don't believe we need the following axiom.
% fof(prev_zero, axiom, prev(zero) = zero).

% Introduce notion of agent:
fof(is_agent, axiom, ![A]: (is_agent(A) <=> (leq(next(zero), A) & leq(A, n)))).

% SYSTEM:

% Store


% iteration
fof(iteration_ptr_before_bot, axiom, ![A]: leq(zero, iteration_ptr_before(A))).
fof(iteration_ptr_before_bot, axiom, ![A]: leq(iteration_ptr_before(A), next(n))).
fof(iteration_ptr_after_bot, axiom, ![A]: leq(zero, iteration_ptr_after(A))).
fof(iteration_ptr_after_bot, axiom, ![A]: leq(iteration_ptr_after(A), next(n))).


fof(state_var_val_0, axiom, state_initial = zero).

fof(state_var_val_1, axiom, state_loop = next(zero)).

fof(state_var_val_2, axiom, state_loop_iterating_1 = next(next(zero))).

fof(state_var_val_3, axiom, state_break = next(next(next(zero)))).

fof(state_var_val_4, axiom, state_crit = next(next(next(next(zero))))).

fof(state_var_val_5, axiom, state_done = next(next(next(next(next(zero)))))).


fof(state_var_values_before, axiom, ![A]: (
  $false
  
  | (state_before(A) = state_initial)
  
  | (state_before(A) = state_loop)
  
  | (state_before(A) = state_loop_iterating_1)
  
  | (state_before(A) = state_break)
  
  | (state_before(A) = state_crit)
  
  | (state_before(A) = state_done)
  
  )).

fof(state_var_values_after, axiom, ![A]: (
  $false
  
  | (state_after(A) = state_initial)
  
  | (state_after(A) = state_loop)
  
  | (state_after(A) = state_loop_iterating_1)
  
  | (state_after(A) = state_break)
  
  | (state_after(A) = state_crit)
  
  | (state_after(A) = state_done)
  
  )).




fof(local_var_val, axiom, b_false = zero).

fof(local_var_val, axiom, b_true = next(zero)).


fof(local_var_values_before, axiom, ![A]: (
  $false
  
  | b_before(A) = b_false
  
  | b_before(A) = b_true
  
  )).

fof(local_var_values_after, axiom, ![A]: (
  $false
  
  | b_after(A) = b_false
  
  | b_after(A) = b_true
  
  )).



% theory pointer
fof(self_ptr, axiom, is_agent(self)).

% transition change
fof(current_state, axiom, state_before(self) = state_loop_iterating_1).
fof(current_state, axiom, state_after(self) = state_loop_iterating_1).

% render guard

fof(comp_1, axiom, other = self).



fof(comp_exceeded, axiom, iteration_ptr_after(self) = next(iteration_ptr_after(self))).


% ensure no change for anything but self

fof(unchanged_b, axiom, ![A]: (A = self | b_after(A) = b_before(A))).

fof(unchanged_state, axiom, ![A]: (A = self | state_after(A) = state_before(A))).


% and for self only state changes (see above)

fof(unchanged_b_self, axiom, b_after(self) = b_before(self)).




fof(mutex_before, axiom, ~(
  ?[P1, P2]: (
    $true
    
    & (
      is_agent(P1)
      
      
      
      & state_before(P1) = state_crit
      
    )
    
    & (
      is_agent(P2)
      
      & leq(P1, P2) & P1 != P2
      
      
      
      & state_before(P2) = state_crit
      
    )
    
    
  )
)).

fof(mutex_after, conjecture, ~(
  ?[P1, P2]: (
    $true
    
    & (
      is_agent(P1)
      
      
      
      & state_after(P1) = state_crit
      
    )
    
    & (
      is_agent(P2)
      
      & leq(P1, P2) & P1 != P2
      
      
      
      & state_after(P2) = state_crit
      
    )
    
    
  )
)).

fof(min_size, axiom, leq(next(next(next(next(zero)))), n)).

% transition change
fof(current_state, axiom, state_before(self) = state_{{ transition.src }}_iterating_{{ transition.index }}).
fof(current_state, axiom, state_after(self) = state_{{ transition.src }}_iterating_{{ transition.index }}).

% render guard
{% for comp in guard %}
fof(comp_{{ loop.index }}, axiom, {{ comp|render_comparison("before") }}).
{% endfor %}

{% if transition.direction == UPWARDS %}
fof(comp_exceeded, axiom, iteration_ptr_after(self) = next(iteration_ptr_after(self))).
{% else %}
fof(comp_exceeded, axiom, iteration_ptr_after(self) = prev(iteration_ptr_after(self))).
{% endif %}

% ensure no change for anything but self
{% for lvar in system.store.local_vars %}
fof(unchanged_{{ lvar.name }}, axiom, ![A]: (A = self | {{ lvar.name }}_after(A) = {{ lvar.name }}_before(A))).
{% endfor %}

% and for self only state changes (see above)
{% for lvar in system.store.local_vars|reject("==", system.store.state_var) %}
fof(unchanged_{{ lvar.name }}_self, axiom, {{ lvar.name }}_after(self) = {{ lvar.name }}_before(self)).
{% endfor %}

{% for ptr in system.store.pointers %}
fof(unchanged_{{ ptr.name }}, axiom, {{ ptr.name }}_after = {{ ptr.name }}_before).
{% endfor %}

import argparse
import pathlib
import csv
import typing
import json
import sys
import subprocess
import datetime

parser = argparse.ArgumentParser()

parser.add_argument("dirs",
                    metavar="<log-dir>",
                    type=pathlib.Path,
                    nargs="+",
                    help="Directories to aggregate.")
parser.add_argument("result",
                    metavar="<result>",
                    type=pathlib.Path,
                    help="Produced .csv file.")

if __name__ == "__main__":
    args = parser.parse_args()
    if args.result.exists():
        if args.result.is_dir():
            print(f"{args.result} is a directory. Aborting...")
            sys.exit(1)
        else:
            answer = input(f"File {args.result} already exists. Override? "
                           f"[y/N]")
            if answer.lower() != "y":
                sys.exit(1)
            else:
                args.result.unlink()
    header = ("file name", "time", "max. N", "no. traps", "no. invariants",
              "max. no. normalized indices", "max. proving time")
    data: typing.List[typing.List[str]] = []
    for log_dir in args.dirs:
        log_file = log_dir / "log.json"
        assert log_file.exists()
        assert log_file.is_file()
        with log_file.open() as log_file_handler:
            log = json.load(log_file_handler)
        log_data = [log["name"], log["time"]]
        log_data.append(str(max((inst["size"]
                                 for obligation in log["obligations"]
                                 for inst in obligation["instances"]),
                                default=0)))
        traps: typing.Set[str] = set()
        invariants: typing.Set[str] = set()
        longest_proof_s: typing.Optional[int] = -1
        for obl in log["obligations"]:
            for inst in obl["instances"]:
                traps |= set(inst["traps"])
            invariants |= set(obl["proof"]["invariants"])
            if longest_proof_s is not None:
                times: typing.List[int] = [longest_proof_s]
                try:
                    proof_begin = datetime.datetime.now()
                    cvc4_result = subprocess.run(
                            [" ".join(["cvc4",
                                       "-q",
                                       "--lang=tptp",
                                       obl["proof"]["tptp-problem"]])],
                            capture_output=True,
                            shell=True,
                            timeout=10)
                    cvc4_proof_time = int((datetime.datetime.now() - proof_begin).total_seconds())
                    proof_begin = datetime.datetime.now()
                    if "SZS status Theorem" in cvc4_result.stdout.decode():
                        times.append(cvc4_proof_time)
                except subprocess.TimeoutExpired:
                    pass
                try:
                    proof_begin = datetime.datetime.now()
                    vampire_result = subprocess.run(
                            [" ".join(["vampire",
                                       "--statistics none",
                                       "--proof off",
                                       "--mode portfolio",
                                       obl["proof"]["tptp-problem"]])],
                            capture_output=True,
                            shell=True,
                            timeout=10)
                    vampire_proof_time = int((datetime.datetime.now() - proof_begin).total_seconds())
                    if "SZS status Theorem" in vampire_result.stdout.decode():
                        times.append(vampire_proof_time)
                except subprocess.TimeoutExpired:
                    pass
                if len(times) == 1:
                    longest_proof_s = None
                else:
                    longest_proof_s = max(times)
        log_data.append(str(len(traps)))
        log_data.append(str(len(invariants)))
        def _how_many_normalized_indices(inv):
            current = 0
            while f"P{current}:" in inv:
                current += 1
            return current
        log_data.append(max(_how_many_normalized_indices(inv)
                            for inv in invariants))
        log_data.append(">10s" if longest_proof_s is None else f"{longest_proof_s}s")
        data.append(log_data)

    with args.result.open("w", newline="") as result_handler:
        writer = csv.writer(result_handler)
        writer.writerow(header)
        for row in data:
            writer.writerow(row)
